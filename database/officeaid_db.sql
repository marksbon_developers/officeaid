-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for officeaid_db
DROP DATABASE IF EXISTS `officeaid_db`;
CREATE DATABASE IF NOT EXISTS `officeaid_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `officeaid_db`;

-- Dumping structure for table officeaid_db.access_login_failed
DROP TABLE IF EXISTS `access_login_failed`;
CREATE TABLE IF NOT EXISTS `access_login_failed` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'auto',
  `user_id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL COMMENT 'the username used for logging in ',
  `password` varchar(255) NOT NULL COMMENT 'encrypted password used for logging in ',
  `user_agent` varchar(255) NOT NULL COMMENT 'contains browser info used to access the system',
  `hostname` varchar(255) NOT NULL COMMENT 'computer name used to access the system',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0' COMMENT 'ip address of the computer',
  `city_region` text COMMENT 'location of the user when accessing the system',
  `country` varchar(255) DEFAULT NULL COMMENT 'country from which the system was accessed',
  `access_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'date of access',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.access_login_failed: 0 rows
/*!40000 ALTER TABLE `access_login_failed` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_login_failed` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.access_login_successful
DROP TABLE IF EXISTS `access_login_successful`;
CREATE TABLE IF NOT EXISTS `access_login_successful` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'auto generated id',
  `user_id` int(5) NOT NULL COMMENT 'auto generated id from users table',
  `time_in` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'time of login by user',
  `time_out` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'time of logout by user',
  `online` int(1) NOT NULL DEFAULT '1' COMMENT 'online status of the user',
  `user_agent` varchar(255) NOT NULL COMMENT 'browser info of the user at the time of system access',
  `ipaddress` varchar(20) NOT NULL DEFAULT '0.0.0.0' COMMENT 'ip address of the user at the time of system access',
  `hostname` varchar(255) NOT NULL COMMENT 'computer name of the user at the time of system access',
  `city_region` text COMMENT 'city & region of the user at the time of system access',
  `country` varchar(255) DEFAULT NULL COMMENT 'country of the user at the time of system access',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.access_login_successful: 1 rows
/*!40000 ALTER TABLE `access_login_successful` DISABLE KEYS */;
INSERT INTO `access_login_successful` (`id`, `user_id`, `time_in`, `time_out`, `online`, `user_agent`, `ipaddress`, `hostname`, `city_region`, `country`) VALUES
	(1, 1, '2020-09-07 08:42:50', '0000-00-00 00:00:00', 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', '127.0.0.1', 'DAEMON', NULL, NULL);
/*!40000 ALTER TABLE `access_login_successful` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.access_password_reset_requests
DROP TABLE IF EXISTS `access_password_reset_requests`;
CREATE TABLE IF NOT EXISTS `access_password_reset_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestor_user_id` int(11) NOT NULL,
  `password_token` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `local_password_token` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `acceptor_user_id` int(11) DEFAULT NULL,
  `status` varchar(45) CHARACTER SET utf8 NOT NULL,
  `date_requested` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.access_password_reset_requests: ~0 rows (approximately)
/*!40000 ALTER TABLE `access_password_reset_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_password_reset_requests` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.access_roles_privileges_group
DROP TABLE IF EXISTS `access_roles_privileges_group`;
CREATE TABLE IF NOT EXISTS `access_roles_privileges_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'name of the group / predefined set of roles & privileges ',
  `roles` text NOT NULL COMMENT 'A set of predefined roles to set a user ',
  `privileges` text NOT NULL COMMENT 'A set of predefined privileges from roles',
  `description` varchar(255) DEFAULT NULL COMMENT 'A summary of text describing that position',
  `login_url` varchar(255) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active' COMMENT 'State of the group whether its active(1) or inactive(0)',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table officeaid_db.access_roles_privileges_group: 5 rows
/*!40000 ALTER TABLE `access_roles_privileges_group` DISABLE KEYS */;
INSERT INTO `access_roles_privileges_group` (`id`, `name`, `roles`, `privileges`, `description`, `login_url`, `status`, `date_created`) VALUES
	(1, 'System Developers', 'New Request|History|Directory|Report|Task|Assign-Ticket-Task|Users|Privileges|Assigned-Tickets|Remittance', '', 'Designers of this software', 'dashboard', 'active', '2017-10-16 22:42:32'),
	(2, 'End Users', 'New Request|History|Directory', '', 'Chief Executive Officer Of The Whole Business', '', 'active', '2019-01-23 05:54:44'),
	(3, 'Assignees', 'New Request|History|Directory|Report|Controls|Assigned-Tickets', '', 'Recieves Clients Warmly And Also The First Point Of Contact. Front Desk Things Too', '', 'active', '2019-01-23 05:56:09'),
	(4, 'Manager', 'New Request|History|Directory|Controls|Report|Task|Assigned|Assign-Ticket-Task', '', NULL, '', 'active', '2019-05-07 11:55:53'),
	(5, 'Administrator', 'New Request|History|Directory|Report|Task|Assign-Ticket-Task|Users|Privileges|Assigned-Tickets|Remittance', '', NULL, '', 'active', '2019-05-07 11:55:53');
/*!40000 ALTER TABLE `access_roles_privileges_group` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.access_roles_privileges_user
DROP TABLE IF EXISTS `access_roles_privileges_user`;
CREATE TABLE IF NOT EXISTS `access_roles_privileges_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL COMMENT 'Contains the id of the user',
  `custom_roles` text CHARACTER SET utf8 NOT NULL COMMENT 'Contains the Exceptional/Added Roles of a user',
  `custom_privileges` text CHARACTER SET utf8 NOT NULL COMMENT 'Contains the Permissible Actions on the Exceptional Roles assigned to the user',
  `group_id` int(11) NOT NULL COMMENT 'Contains the id of the group to which the user belongs to in roles & priviledges',
  `status` enum('active','inactive','deleted','') CHARACTER SET utf8 DEFAULT 'active' COMMENT 'state of the user''s permissions.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table officeaid_db.access_roles_privileges_user: 52 rows
/*!40000 ALTER TABLE `access_roles_privileges_user` DISABLE KEYS */;
INSERT INTO `access_roles_privileges_user` (`id`, `user_id`, `custom_roles`, `custom_privileges`, `group_id`, `status`) VALUES
	(1, 1, '', '', 1, 'active'),
	(4, 12, '', '', 4, 'active'),
	(5, 13, '', '', 5, 'active'),
	(8, 16, '', '', 2, 'active'),
	(9, 2, '', '', 5, 'active'),
	(10, 4, '', '', 3, 'active'),
	(11, 5, '', '', 3, 'active'),
	(12, 6, '', '', 4, 'active'),
	(13, 7, '', '', 3, 'active'),
	(14, 8, '', '', 4, 'active'),
	(15, 9, '', '', 2, 'active'),
	(16, 10, '', '', 2, 'active'),
	(17, 11, '', '', 2, 'active'),
	(18, 14, '', '', 2, 'active'),
	(19, 15, '', '', 2, 'active'),
	(21, 17, '', '', 2, 'active'),
	(22, 18, '', '', 2, 'active'),
	(23, 19, '', '', 2, 'active'),
	(24, 20, '', '', 2, 'active'),
	(25, 21, '', '', 4, 'active'),
	(26, 22, '', '', 3, 'active'),
	(27, 23, '', '', 4, 'active'),
	(28, 24, '', '', 4, 'active'),
	(29, 25, '', '', 4, 'active'),
	(30, 26, '', '', 3, 'active'),
	(31, 27, '', '', 3, 'active'),
	(32, 28, '', '', 4, 'active'),
	(33, 29, '', '', 4, 'active'),
	(34, 30, '', '', 4, 'active'),
	(35, 31, '', '', 3, 'active'),
	(36, 32, '', '', 4, 'active'),
	(37, 33, '', '', 4, 'active'),
	(38, 34, '', '', 4, 'active'),
	(39, 35, '', '', 4, 'active'),
	(40, 36, '', '', 2, 'active'),
	(41, 37, '', '', 2, 'active'),
	(42, 38, '', '', 3, 'active'),
	(43, 39, '', '', 4, 'active'),
	(44, 40, '', '', 3, 'active'),
	(45, 41, '', '', 3, 'active'),
	(46, 42, '', '', 2, 'active'),
	(47, 43, '', '', 2, 'active'),
	(48, 44, '', '', 4, 'active'),
	(49, 45, '', '', 4, 'active'),
	(50, 46, '', '', 2, 'active'),
	(51, 47, '', '', 2, 'active'),
	(52, 48, '', '', 4, 'active'),
	(53, 49, '', '', 5, 'active'),
	(54, 50, '', '', 2, 'active'),
	(55, 51, '', '', 2, 'active'),
	(56, 52, '', '', 2, 'active'),
	(57, 53, '', '', 4, 'active');
/*!40000 ALTER TABLE `access_roles_privileges_user` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.access_users
DROP TABLE IF EXISTS `access_users`;
CREATE TABLE IF NOT EXISTS `access_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `passwd` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `phone_number` varchar(20) CHARACTER SET utf8 NOT NULL,
  `department_id` tinyint(1) NOT NULL,
  `login_attempt` tinyint(1) NOT NULL DEFAULT '5',
  `status` enum('active','inactive','deleted','') CHARACTER SET utf8 NOT NULL DEFAULT 'active',
  `created_by` bigint(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table officeaid_db.access_users: ~50 rows (approximately)
/*!40000 ALTER TABLE `access_users` DISABLE KEYS */;
INSERT INTO `access_users` (`id`, `username`, `passwd`, `fullname`, `phone_number`, `department_id`, `login_attempt`, `status`, `created_by`, `date_created`) VALUES
	(1, 'marksbonapps@gmail.com', '$2y$10$O/swrfsJ6TbhtHiyzR7GmurgN4u49VcaMFzrOtZ9.3N511hoPhOVi', 'Osborne Mordreds', '0541786220', 1, 5, 'active', 0, '2018-07-03 15:59:37'),
	(2, 'boffei@okomfoanokyeruralbank.com', '$2y$10$utWsuKpSfO3vwDob7j8W4OksH6I4j5zSGfp5GHxKvjO4G3XyE171e', 'Bimark Offei', '0501628238', 1, 5, 'deleted', 1, '2020-07-10 14:11:05'),
	(4, 'bisoffei@okomfoanokyeruralbank.com', '$2y$10$jlottx/rhjB6j5wSl/PXB./ZexHbpMB4uL.dsj3yONziI70V4LzRG', 'Bismark Offei', '0501628238', 1, 5, 'active', 1, '2020-07-10 15:13:16'),
	(5, 'kelvin.niako@okomfoanokyerural.com', '$2y$10$7XknsVWe5rCgL8r23O1NB.5aFK8L09m3/M62G4BGjZYqG8uDOEx66', 'Kelvin Niako', '0504344932', 3, 5, 'active', 4, '2020-07-10 15:27:27'),
	(6, 'gcoffie@okomfoanokyeruralbank.com', '$2y$10$/XQf0EKylowPP8PvOShkWOpR155mns8UpxL4JlhbLcspkfImVlUIK', 'George Coffie', '0204344997', 1, 5, 'active', 4, '2020-07-13 08:28:19'),
	(7, 'kbtwum@okomfoanokyeruralbank.com', '$2y$10$JRJbJkjj2NeJQoffHAGF0./juXmMlZEECuwj2rbnd2xEnntIJNb6C', 'Kwasi Twum Boakye', '0204344928', 3, 5, 'active', 4, '2020-07-13 08:34:50'),
	(8, 'samuel.bosei@okomfoanokyeruralbank.com', '$2y$10$wAk9NBtLbByot.IxUoMXI.u/hc5kgTE6e1ZcAXi/zao2n33FQ2.9i', 'Samuel Osei Boahen', '0204344942', 1, 5, 'active', 4, '2020-07-13 08:48:58'),
	(9, 'godfred.yeboah@okomfoanokyeruralbank.com', '$2y$10$YnnDOZPIvKC7vpEfFTpxbehLm2bfJNfAEQYBeUe5/jPKvqc0avryC', 'Godfred Yeboah', '0204344925', 3, 5, 'active', 4, '2020-07-13 08:52:48'),
	(10, 'okw@okomfoanokyeruralbank.com', '$2y$10$HSRWquujrP2cjbXbP9KrzucEuTXWqpFQN7QnaL5oNE.R2jytw8C76', 'WIAMOASI', '0204344925', 7, 5, 'active', 1, '2020-07-13 12:11:02'),
	(11, 'okg@okomfoanokyeruralbank.com', '$2y$10$HHrxeIisw56YqNudc6oUkuvMfDZnfKbbm9BhF4Zeworza8aoOGObG', 'AGONA', '0501301266', 8, 5, 'active', 1, '2020-07-13 12:15:08'),
	(14, 'okgg@okomfoanokyeruralbank.com', '$2y$10$L1XsHm0ie4jjm0sRUm8cteg2TycWoSKZyusJepy6u0m4vUxFqsU6.', 'AGONA', '0501301266', 8, 5, 'deleted', 1, '2020-07-13 12:16:57'),
	(15, 'okp@okomfoanokyeruralbank.com', '$2y$10$EMNuKVS6QvN5IbPwbVysd.Qg5B2.SvLg4KDlh0dLzxLg.9m1hdS7C', 'PANKRONO', '0204344914', 14, 5, 'active', 1, '2020-07-13 13:22:59'),
	(16, 'oka@okomfoanokyeruralbank.com', '$2y$10$9ngu0NqTCrv3Ti9k1I6IpevlRXVy1UX1Ax0diOT5MOsF0ZHOCmX.q', 'ASH-TOWN', '0204344916', 13, 5, 'active', 1, '2020-07-13 13:29:09'),
	(17, 'okk@okomfoanokyeruralbank.com', '$2y$10$Rzrq3TmMzvLLZfO8N5TmBe2kaEDj19YRS75OoHlZ7QEWHJOOzf5KS', 'KRONUM', '0204344911', 15, 5, 'active', 1, '2020-07-13 13:31:15'),
	(18, 'oks@okomfoanokyeruralbank.com', '$2y$10$UDV/7QOXbMZxFNJc0ovmOOoPD8pGWfubrjpHdeUH0mMzQoOomTTWy', 'SUAME', '0204344908', 12, 5, 'active', 1, '2020-07-13 13:32:50'),
	(19, 'okb@okomfoanokyeruralbank.com', '$2y$10$MDdW8xzCGSJOaeyUpKuK6.ozGfDjtQ4aZyiCqnarfnoDGs4DqkbrG', 'BOAMANG', '0204344908', 10, 5, 'active', 1, '2020-07-13 13:33:48'),
	(20, 'okt@okomfoanokyeruralbank.com', '$2y$10$29S/CK.SPapF7rRRQ8DKueVmoH6pzv6R1CYh7g6eWJP8O16I5M.7W', 'TETREM', '0204344939', 9, 5, 'active', 1, '2020-07-13 13:35:06'),
	(21, 'jkaddae@okomfoanokyeruralbank.com', '$2y$10$HRnoG5PesNE3XLmy2c045OoptPIxdXLRLilJ3lgHlSQpACO2ACDva', 'Joseph Addae', '0248811018', 17, 5, 'active', 1, '2020-07-14 10:24:18'),
	(22, 'aeanokye@okomfoanokyeruralbank.com', '$2y$10$KrYBi6P4HumfR17HIILGae2m8P.MemnAYtxmkr24rLe/DKGbTptwm', 'EDWARD AMOH', '0501628238', 17, 5, 'active', 1, '2020-07-14 11:02:12'),
	(23, 'pkoduro@okomfoanokyeruralbank.com', '$2y$10$jgGcWEC1.OXTueIi6om2C.sNy8ku6EHR1J3yWJRl/TC37NkqMgkr.', 'Paul Kwabina Oduro', '0501628452', 20, 5, 'active', 1, '2020-07-14 17:01:54'),
	(24, 'mavis.aopoku@okomfoanokyeruralbank.com', '$2y$10$deSSlCMaS5l/lIKMg6IFHewQQBmjltY1AJabWmXjs9YdKaQF6DQ.2', 'MAVIS OPOKU AGYEMANG', '0204344917', 18, 5, 'active', 1, '2020-07-15 08:22:13'),
	(25, 'samuel.doffei@okomfoanokyeruralbank.com', '$2y$10$5RC7c9oV2aX0TPGB43.ese26l3L9.2wBfzIr/EuLOhcBxtTFnCO0a', 'SAMUEL DANQUAH', '0501294803', 19, 5, 'active', 1, '2020-07-17 10:41:44'),
	(26, 'eoasare@okomfoanokyeruralbank.com', '$2y$10$8Gxsj6THBHVJz5VFv8277O/wx95E6hAI40p0Ichr1Vw3rCss8oAFm', 'EUNICE OWUSU ASARE', '0501628240', 19, 5, 'active', 1, '2020-07-17 11:44:05'),
	(27, 'ekobea@okomfoanokyeruralbank.com', '$2y$10$fE/NCwkfD7sAF6HwZKS/zuQoTQbEgrU8kMHHB4a5.wR2UC1JPDnze', 'EMMANUEL KOBEA', '0246371654', 4, 5, 'active', 1, '2020-07-20 11:54:27'),
	(28, 'kwasi.bempa@okomfoanokyeruralbank.com', '$2y$10$1hwaq3mslJ6QAoGKaS7i7./1T.YcKvuhk8RdwTZFiWJrAlQApSd1e', 'KWASI BEMPAH', '0242088392', 4, 5, 'active', 1, '2020-07-21 09:51:50'),
	(29, 'cbrobbey@okomfoanokyeruralbank.com', '$2y$10$SlGx1NCnYWTFZUXFlzSPT.SYxXQCwyMCBRWtQRtcKOxpd1KyCKCIq', 'CHARLES BROBBEY', '0204344916', 13, 5, 'active', 1, '2020-07-22 07:49:43'),
	(30, 'emmanuel.oppong@okomfoanokyeruralbank.com', '$2y$10$ufcpgakPwKGeLxqZwKZ13.pPa4ZgSrgAAbKK2jiHCSGgJ6fG5g8o6', 'EMMANUEL OPPONG', '0204344908', 12, 5, 'active', 1, '2020-07-22 07:51:41'),
	(31, 'katwene@okomfoanokyeruralbank.com', '$2y$10$k2WcYUTTa8fzy937klnLn.D4HJAVDd28BEvQtcDOLCV62fQimrONy', 'KOFI A ATWENE', '0246735650', 12, 5, 'active', 1, '2020-07-22 07:54:32'),
	(32, 'victoria.ohene@okomfoanokyeruralbank.com', '$2y$10$c5kMrK92wXa5U5k5bY6vzu..68lhYhtvpAR3PmyXZdEkAH4Uuq6v.', 'VICTORIA OHENE', '0501682806', 9, 5, 'active', 1, '2020-07-22 07:56:11'),
	(33, 'syessel@okomfoanokyeruralbank.com', '$2y$10$trPSTcvLIOmun8cl5g9kv.LwApMTPHbpECJwXrk94TBq43cGecpSa', 'SAMUEL YAW  ESSEL', '0204344914', 14, 5, 'active', 1, '2020-07-22 07:58:27'),
	(34, 'kankapong@okomfoanokyeruralbank.com', '$2y$10$pAO3MN6nnaxzODtwlq1fpeG8aXNb1UxUSFO4DlzXCLWZTCuw9jyfS', 'KWAME ANKAPONG', '0204344911', 15, 5, 'active', 1, '2020-07-22 08:01:07'),
	(35, 'eatwumasi@okomfoanokyeruralbank.com', '$2y$10$61SUZO6taxU5KKgMkrXyUes6D87PjkYe2CEpfglqJ5AMolgyMHeOC', 'EVANS TWUMASI', '0501301266', 8, 5, 'active', 1, '2020-07-22 08:03:01'),
	(36, 'leticia.bansah@okomfoanokyeruralbank.com', '$2y$10$kv1xBrCDNfKDbN9e/S2H/OdFsm1hEGAgNvbpk/bDdTkoL4DdxAzmW', 'LETICIA B ANSAH', '0204344912', 2, 5, 'active', 1, '2020-07-22 14:29:29'),
	(37, 'eaboasiako@okomfoanokyeruralbank.com', '$2y$10$KaQqdakj7f8yAJ5WbIpyPehrjRRGv9.JVGmKdcm3CANbpcO.4Dygm', 'EMMANUEL A BOASIAKO', '0245787676', 8, 5, 'active', 1, '2020-07-23 08:10:58'),
	(38, 'doris.anokye@okomfoanokyeruralbank.com', '$2y$10$rwcnudKKtSCBD7Db/XNJTOBLd.CABhCepaCiiB/FFFclmDvyWOVQS', 'DORIS ANOKYE', '0208755979', 9, 5, 'active', 1, '2020-07-23 13:21:16'),
	(39, 'daniel.aboakye@okomfoanokyeruralbank.com', '$2y$10$fxTxleXhXNdRqeC9yKldNegxGuIkY0PdFYWEYC9b8c1pgvdfk.Zu2', 'DANIEL A BOAKYE', '0249469649', 9, 5, 'active', 1, '2020-07-23 13:50:34'),
	(40, 'osei.boamah@okomfoanokyeruralbank.com', '$2y$10$gcuw0Spq3Hk7kakdliTgOOKRVJKvZrGNvdrQEAuppUiIwWHtwba6i', 'OSEI BOAMAH', '0501319284', 9, 5, 'active', 1, '2020-07-23 13:54:43'),
	(41, 'abraham.tnimoh@okomfoanokyeruralbank.com', '$2y$10$IDv7fAf9cEgXBHHdsRxFQ.L1XxsMU2.67zZa3BOwBtOwyCR0E9vni', 'ABRAHAM N TAKYI', '0509154958', 10, 5, 'active', 1, '2020-07-24 11:13:39'),
	(42, 'gwilliams@okomfoanokyeruralbank.com', '$2y$10$fYrSGpgl6P9vVzoKs06KEuoCqCYMV/Ox4MX3A5H9p.reFI3jpbQWO', 'GYEDU WILLIAMS', '0501647568', 15, 5, 'active', 1, '2020-07-27 08:45:49'),
	(43, 'kboakye@okomfoanokyeruralbank.com', '$2y$10$FZQOqDjsncxvbxo1D3NfgeNjlW.soq/JSxEuWEgvTAI3arPChGPNK', 'KWADJO BOAKYE', '0245095185', 8, 5, 'active', 1, '2020-07-27 09:25:41'),
	(44, 'bismark.bfrimpong@okomfoanokyeruralbank.com', '$2y$10$8CvLeiXY8JZ37DNLzH1VieHnEFSC5qjC4fan6qvtbLSeBmln3u9YW', 'BISMARK F BOAMAH', '0204344913', 3, 5, 'active', 1, '2020-07-27 09:56:59'),
	(45, 'isaiah.ameyaw-amankwah@okomfoanokyeruralbank.com', '$2y$10$ZybeTk8G7dWguVaCqdGdOuniv2g7uZ7TtEfYjfGqr93PbGBlrV6uy', 'ISAIAH A AMANKWAH', '0204344993', 3, 5, 'active', 1, '2020-07-27 09:59:49'),
	(46, 'ekgyasi@okomfoanokyeruralbank.com', '$2y$10$V0FmPcz92mUDqS2Gm.DppOgQtvzRVUS5PJJg/3omi.z5UXzAQpjK2', 'EDWARD K GYASI', '0246492880', 3, 5, 'active', 1, '2020-07-28 09:45:44'),
	(47, 'makyamfour@okomfoanokyeruralbank.com', '$2y$10$gn1M4LkW5.Qu9QvzwAKg8.vnbFoNzcYu/56Qz0jz66mO0f.D1hD/G', 'MARTIN O AKYAMFOUR', '0249434488', 3, 5, 'active', 1, '2020-07-28 09:50:39'),
	(48, 'prince.aowusu@okomfoanokyeruralbank.com', '$2y$10$mOIVpLlUcyEhsg3Dxm3nFutHtQlnuKIdKP1/ptamih7d7OVPmv/q6', 'PRINCE A OWUSU', '0541817556', 4, 5, 'active', 1, '2020-07-28 16:49:43'),
	(49, 'officeaid@marksbon.com', '$2y$10$4OkGIPRk9iZc04fI.NqASuxm8QL2Y504QRL3rGoRdBvsABLyDHsz6', 'Officeaid', '0245626487', 1, 5, 'active', 1, '2020-07-30 12:04:29'),
	(50, 'gabriel.oasare@okomfoanokyeruralbank.com', '$2y$10$mn6UjJbtbdY.RxrHW.c0CeGpAtA9cr.w4HrJpzon.Yjui3VZGA2WG', 'GABRIEL OWUSU ASARE', '0204344930', 8, 5, 'active', 1, '2020-08-05 08:36:55'),
	(51, 'portia.agyei-asantewaa@okomfoanokyeruralbank.com', '$2y$10$2iAzLyCobH7cEKaMB4.bY.WVkA0G7Nz5YmBupzVc6Eaz/hWZ4t7cS', 'PORTIA A ASANTEWAA', '0204344930', 8, 5, 'active', 1, '2020-08-05 08:49:05'),
	(52, 'pokarikari@okomfoanokyeruralbank.com', '$2y$10$LYldWKXHgSsl.m2Kc84EZuxaxTEYPOk6BF4nwkE9cci7T57lytP..', 'PETRE O KARIKARI', '0203301158', 10, 5, 'active', 1, '2020-08-05 10:09:01'),
	(53, 'mabako@okomfoanokyeruralbank.com', '$2y$10$7fCK46/wvoJgd/2pojA9YOTFua8bcQ4a9wYQ.MURMm5dfGKmpInmS', 'MARVIN ABAKO', '0242073542', 5, 5, 'active', 1, '2020-08-06 13:56:33');
/*!40000 ALTER TABLE `access_users` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.company_info
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE IF NOT EXISTS `company_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `telephone_1` varchar(20) NOT NULL,
  `telephone_2` varchar(20) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `postal_address` varchar(255) NOT NULL,
  `residence_address` varchar(255) NOT NULL COMMENT 'physical location and street name where company is located',
  `website` varchar(100) DEFAULT NULL COMMENT 'website of the comany',
  `mission` varchar(255) DEFAULT NULL,
  `vision` varchar(255) DEFAULT NULL,
  `gps_location` varchar(255) DEFAULT NULL,
  `tin_number` varchar(50) DEFAULT NULL,
  `logo_path` varchar(50) NOT NULL,
  `date_of_commence` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table officeaid_db.company_info: 1 rows
/*!40000 ALTER TABLE `company_info` DISABLE KEYS */;
INSERT INTO `company_info` (`id`, `name`, `telephone_1`, `telephone_2`, `fax`, `email`, `postal_address`, `residence_address`, `website`, `mission`, `vision`, `gps_location`, `tin_number`, `logo_path`, `date_of_commence`) VALUES
	(1, 'Okomfo Anokye Rural Bank', '+233 0204344906', '0246543708', NULL, 'Info@okomfoanokyeruralbank.com', 'Okomfo Anokye Rural Bank Ltd\r\nPostal Office Box 13\r\nWiamoase Ashanti\r\nKumasi', 'Kumasi-Wiamoase', 'www.okomfoanokyeruralbank.com', 'MISSION STATEMENT', 'VISION STATEMAN', 'GA-125-69631', 'TN-12345678', 'resources/img/logo.png', '0000-00-00');
/*!40000 ALTER TABLE `company_info` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.complains
DROP TABLE IF EXISTS `complains`;
CREATE TABLE IF NOT EXISTS `complains` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table officeaid_db.complains: ~7 rows (approximately)
/*!40000 ALTER TABLE `complains` DISABLE KEYS */;
INSERT INTO `complains` (`id`, `name`, `status`, `date_created`, `createdby`) VALUES
	(1, 'T24', 'active', '2019-05-07 21:53:44', 0),
	(2, 'REMITTANCE', 'active', '2019-05-07 21:54:05', 0),
	(3, 'NETWORK', 'active', '2019-05-07 21:54:28', 0),
	(4, 'GVIVE/OTHER APPS', 'active', '2019-05-07 21:54:37', 0),
	(5, 'HARDWARE / PRINTERS', 'active', '2019-05-07 21:55:08', 0),
	(6, 'WORKSTATION', 'active', '2019-05-07 21:55:31', 0),
	(7, 'OTHERS', 'active', '2019-05-07 21:55:42', 0);
/*!40000 ALTER TABLE `complains` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.departments
DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.departments: ~20 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `name`, `description`, `type`, `status`, `date_created`) VALUES
	(1, 'Info. Tech. (IT)', 'Information Technology', 1, 'active', '2018-12-30 13:53:15'),
	(2, 'Client Service', 'Clients service', 1, 'active', '2018-12-30 13:53:15'),
	(3, 'Operations', '', 1, 'active', '2018-12-30 13:53:15'),
	(4, 'Finance', '', 1, 'active', '2018-12-30 13:53:15'),
	(5, 'Human Resource', '', 1, 'active', '2018-12-30 13:53:15'),
	(6, 'OKH', '', 2, 'active', '2019-04-30 08:30:36'),
	(7, 'OKW', '', 2, 'active', '2019-04-30 08:30:36'),
	(8, 'OKG', '', 2, 'active', '2019-04-30 08:30:36'),
	(9, 'OKT', '', 2, 'active', '2019-04-30 08:30:36'),
	(10, 'OKB', '', 2, 'active', '2019-04-30 08:30:36'),
	(11, 'OKF', '', 2, 'active', '2019-04-30 08:30:36'),
	(12, 'OKS', '', 2, 'active', '2019-04-30 08:30:36'),
	(13, 'OKA', '', 2, 'active', '2019-04-30 08:30:36'),
	(14, 'OKP', '', 2, 'active', '2019-04-30 08:30:36'),
	(15, 'OKK', '', 2, 'active', '2019-04-30 08:30:36'),
	(16, 'Compliance', 'Compliance', 1, 'active', '2018-12-30 13:53:15'),
	(17, 'Credit', 'Credit', 1, 'active', '2018-12-30 13:53:15'),
	(18, 'Administration', 'Administration', 1, 'active', '2018-12-30 13:53:15'),
	(19, 'Audit', 'Audit', 1, 'active', '2018-12-30 13:53:15'),
	(20, 'GM', 'GM', 1, 'active', '2018-12-30 13:53:15');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.department_positions
DROP TABLE IF EXISTS `department_positions`;
CREATE TABLE IF NOT EXISTS `department_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  `position_id` int(11) NOT NULL,
  `date_attained` datetime NOT NULL,
  `date_released` datetime NOT NULL,
  `action_by` int(11) NOT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.department_positions: ~0 rows (approximately)
/*!40000 ALTER TABLE `department_positions` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_positions` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone_1` varchar(20) NOT NULL,
  `phone_2` varchar(20) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT 'personal',
  `subject` varchar(191) NOT NULL,
  `createdby` int(11) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `department_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `filecode` varchar(255) DEFAULT NULL,
  `filepath` varchar(191) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table officeaid_db.files: ~0 rows (approximately)
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.files_additions
DROP TABLE IF EXISTS `files_additions`;
CREATE TABLE IF NOT EXISTS `files_additions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(191) NOT NULL,
  `createdby` int(11) NOT NULL,
  `filetype` varchar(50) NOT NULL,
  `department_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `filecode` varchar(255) DEFAULT NULL,
  `filepath` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table officeaid_db.files_additions: ~0 rows (approximately)
/*!40000 ALTER TABLE `files_additions` DISABLE KEYS */;
/*!40000 ALTER TABLE `files_additions` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.groups
DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table officeaid_db.groups: ~2 rows (approximately)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
	(1, 'admin', 'Administrator'),
	(2, 'members', 'General User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.license_keys
DROP TABLE IF EXISTS `license_keys`;
CREATE TABLE IF NOT EXISTS `license_keys` (
  `id` int(11) NOT NULL,
  `key` int(11) NOT NULL,
  `valid_till` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.license_keys: ~0 rows (approximately)
/*!40000 ALTER TABLE `license_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `license_keys` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.positions
DROP TABLE IF EXISTS `positions`;
CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.positions: ~0 rows (approximately)
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.remittance
DROP TABLE IF EXISTS `remittance`;
CREATE TABLE IF NOT EXISTS `remittance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `title` varchar(10) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `initials` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `marital_status` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL COMMENT 'physical location and street name where company is located',
  `street_address` varchar(100) DEFAULT NULL COMMENT 'website of the comany',
  `postal_address` varchar(100) DEFAULT NULL,
  `primary_tel` varchar(255) DEFAULT NULL,
  `secondary_tel` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `region` varchar(50) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `id_type` varchar(50) DEFAULT NULL,
  `id_number` varchar(50) DEFAULT NULL,
  `date_issued` date DEFAULT NULL,
  `date_expiry` date DEFAULT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `contact_phone` varchar(50) DEFAULT NULL,
  `contact_address` varchar(100) DEFAULT NULL,
  `contact_enumber` varchar(50) DEFAULT NULL,
  `employment_type` varchar(50) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `employer_name` varchar(100) DEFAULT NULL,
  `employer_address` varchar(100) DEFAULT NULL,
  `employment_region` varchar(50) DEFAULT NULL,
  `staff_id` varchar(50) DEFAULT NULL,
  `date_of_employement` date DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `account_num_1` varchar(50) DEFAULT NULL,
  `account_num_2` varchar(50) DEFAULT NULL,
  `withdrawal_limit` varchar(50) DEFAULT NULL,
  `average_income` varchar(50) DEFAULT NULL,
  `linked_bank_name_1` varchar(50) DEFAULT NULL,
  `linked_account_name_1` varchar(50) DEFAULT NULL,
  `linked_account_number_1` varchar(50) DEFAULT NULL,
  `linked_account_type_1` varchar(50) DEFAULT NULL,
  `linked_bank_name_2` varchar(50) DEFAULT NULL,
  `linked_account_name_2` varchar(50) DEFAULT NULL,
  `linked_account_number_2` varchar(50) DEFAULT NULL,
  `linked_account_type_2` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table officeaid_db.remittance: 0 rows
/*!40000 ALTER TABLE `remittance` DISABLE KEYS */;
/*!40000 ALTER TABLE `remittance` ENABLE KEYS */;

-- Dumping structure for table officeaid_db.requests
DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `sender_contact` varchar(20) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `priority` enum('low','medium','high','urgent') NOT NULL DEFAULT 'low',
  `type` varchar(50) NOT NULL DEFAULT 'ticket',
  `complain_id` tinyint(4) NOT NULL DEFAULT '0',
  `duedate` date NOT NULL,
  `file_id` bigint(20) NOT NULL,
  `department_id` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `updated_at` bigint(20) NOT NULL,
  `assigned_to` bigint(20) NOT NULL,
  `date_solved` datetime NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table officeaid_db.requests: ~0 rows (approximately)
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;

-- Dumping structure for view officeaid_db.vw_company_info
DROP VIEW IF EXISTS `vw_company_info`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vw_company_info` 
) ENGINE=MyISAM;

-- Dumping structure for view officeaid_db.vw_files
DROP VIEW IF EXISTS `vw_files`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vw_files` (
	`id` BIGINT(20) UNSIGNED NOT NULL,
	`type` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`subject` VARCHAR(191) NOT NULL COLLATE 'utf8_general_ci',
	`createdby` INT(11) NOT NULL,
	`filetype` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`department_id` INT(11) NOT NULL,
	`status` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`filecode` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`filepath` VARCHAR(191) NOT NULL COLLATE 'utf8_general_ci',
	`date_created` TIMESTAMP NOT NULL,
	`fullname` VARCHAR(255) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view officeaid_db.vw_requests
DROP VIEW IF EXISTS `vw_requests`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vw_requests` (
	`id` BIGINT(20) NOT NULL,
	`email` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`created_by` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`created_by_department` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`sender_contact` VARCHAR(20) NOT NULL COLLATE 'latin1_swedish_ci',
	`subject` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci',
	`description` MEDIUMTEXT NOT NULL COLLATE 'latin1_swedish_ci',
	`priority` ENUM('low','medium','high','urgent') NOT NULL COLLATE 'latin1_swedish_ci',
	`file_id` BIGINT(20) NOT NULL,
	`assigned_to` BIGINT(20) NOT NULL,
	`status` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`duedate` DATE NOT NULL,
	`date_created` DATETIME NOT NULL,
	`date_solved` DATETIME NOT NULL,
	`assignee` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`department_id` INT(11) NULL,
	`department` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`request_department` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`request_department_id` INT(11) NULL,
	`type` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`complain_id` TINYINT(4) NOT NULL,
	`complain` VARCHAR(191) NULL COLLATE 'utf8_general_ci',
	`filepath` VARCHAR(191) NULL COLLATE 'utf8_general_ci',
	`filetype` VARCHAR(50) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;

-- Dumping structure for view officeaid_db.vw_user_details
DROP VIEW IF EXISTS `vw_user_details`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `vw_user_details` (
	`id` BIGINT(20) NOT NULL,
	`username` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`passwd` VARCHAR(100) NOT NULL COLLATE 'utf8_general_ci',
	`fullname` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`phone_number` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`department_id` TINYINT(1) NOT NULL,
	`login_attempt` TINYINT(1) NOT NULL,
	`status` ENUM('active','inactive','deleted','') NOT NULL COLLATE 'utf8_general_ci',
	`created_by` BIGINT(20) NOT NULL,
	`date_created` TIMESTAMP NOT NULL,
	`custom_roles` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`custom_privileges` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`group_id` VARCHAR(11) NULL COLLATE 'utf8mb4_general_ci',
	`user_roles_status` VARCHAR(8) NULL COLLATE 'utf8_general_ci',
	`group_name` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`group_roles` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`group_privileges` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`group_login_url` VARCHAR(255) NULL COLLATE 'utf8_general_ci',
	`department` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;

-- Dumping structure for view officeaid_db.vw_company_info
DROP VIEW IF EXISTS `vw_company_info`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vw_company_info`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_company_info` AS select `hr_company_info`.`id` AS `id`,`hr_company_info`.`name` AS `name`,`hr_company_info`.`telephone_1` AS `telephone_1`,`hr_company_info`.`telephone_2` AS `telephone_2`,`hr_company_info`.`fax` AS `fax`,`hr_company_info`.`email` AS `email`,`hr_company_info`.`postal_address` AS `postal_address`,`hr_company_info`.`residence_address` AS `residence_address`,`hr_company_info`.`website` AS `website`,`hr_company_info`.`mission` AS `mission`,`hr_company_info`.`vision` AS `vision`,`hr_company_info`.`gps_location` AS `gps_location`,`hr_company_info`.`tin_number` AS `tin_number`,`hr_company_info`.`logo_id` AS `logo_id`,`hr_company_info`.`date_of_commence` AS `date_of_commence` from `hr_company_info` ;

-- Dumping structure for view officeaid_db.vw_files
DROP VIEW IF EXISTS `vw_files`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vw_files`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_files` AS SELECT a.*,b.fullname FROM files a

LEFT JOIN access_users b ON b.id = a.createdby ;

-- Dumping structure for view officeaid_db.vw_requests
DROP VIEW IF EXISTS `vw_requests`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vw_requests`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_requests` AS select 
  `a`.`id` AS `id`,
  `a`.`email` AS `email`,
  user.fullname as created_by,
  dept.name as created_by_department,
  `a`.`sender_contact` AS `sender_contact`,
  `a`.`subject` AS `subject`,
  `a`.`description` AS `description`,
  `a`.`priority` AS `priority`,
  `a`.`file_id` AS `file_id`,
  `a`.`assigned_to` AS `assigned_to`,
  `a`.`status` AS `status`,
  a.duedate,
  `a`.`date_created` AS `date_created`,
  `a`.`date_solved` AS `date_solved`,
  `ass`.`fullname` AS `assignee`,
  dept.id as department_id, 
  dept.name as department, 
  req_dept.name AS request_department,
  req_dept.id AS request_department_id,
  a.type, 
  a.`complain_id`,
  comp.name as complain,
  file.filepath, 
  file.filetype

from ((
  `requests` `a` 
  
left join `access_users` `user` on((user.username = a.email))
left join `access_users` `ass` on((ass.id = a.assigned_to))
left join  departments dept on((dept.id = user.department_id))
left join  files file on((file.id = a.file_id))
left join complains comp on comp.id = a.complain_id
left join  departments req_dept on((req_dept.id = a.department_id))
)) ;

-- Dumping structure for view officeaid_db.vw_user_details
DROP VIEW IF EXISTS `vw_user_details`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `vw_user_details`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_user_details` AS SELECT
    `a`.`id` AS `id`,
    `a`.`username` AS `username`,
    `a`.`passwd` AS `passwd`,
    a.fullname,
    `a`.`phone_number`,
    `a`.`department_id` AS `department_id`,
`a`.`login_attempt` AS `login_attempt`,
`a`.`status` AS `status`,
`a`.`created_by` AS `created_by`,
`a`.`date_created` AS `date_created`,
COALESCE(`b`.`custom_roles`, '') AS `custom_roles`,
COALESCE(`b`.`custom_privileges`, '') AS `custom_privileges`,
COALESCE(`b`.`group_id`, '') AS `group_id`,
COALESCE(`b`.`status`, '') AS `user_roles_status`,
COALESCE(`c`.`name`, '') AS `group_name`,
COALESCE(`c`.`roles`, '') AS `group_roles`,
COALESCE(`c`.`privileges`, '') AS `group_privileges`,
`c`.`login_url` AS `group_login_url`,
dept.name as department
FROM
    (
        (
            (
                `access_users` `a`
            LEFT JOIN `access_roles_privileges_user` `b` ON
                ((`a`.`id` = `b`.`user_id`))
            )
        LEFT JOIN `access_roles_privileges_group` `c` ON
            ((`b`.`group_id` = `c`.`id`))
            
            left join departments dept on dept.id = a.department_id
        )
    ) ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
