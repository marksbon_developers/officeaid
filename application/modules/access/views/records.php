<div class="card">
    <div class="card-body">
        <table id="dt-example-responsive" class="table table-bordered" >
          <thead>
            <tr>
              <th>No #</th>
              <th>Full Name</th>
              <th>DOB</th>
              <th>Gender</th>
              <th> Region</th>
              <th>Tel.</th>
              <th>Address</th>
              <th>C.Person</th>
              <th>E-Zwich</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>BISMARK</td>
              <td>11/12/20</td>
              <td>MALE</td>
              <td>ASHANTI</td>
              <td>021555888</td>
              <td>P O BOX TA353, TAIFA</td>
              <td>GEORGE</td>
              <td>62548-9</td>
              <td>
                <button type="button" class="btn btn-block btn-xs btn-block">Edit</button>
                <button type="button" class="btn btn-block btn-xs btn-block">Print</button>
               
              </td>
            </tr>
          </tbody>
        </table>


      <!--   <script type="text/javascript">
          $(document).ready(function(){
            /************** Default Settings **************/
              $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                responsive: true,
                columnDefs: [{ 
                    orderable: false,
                    width: '100px',
                }],
                dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                  search: '<span>Filter:</span> _INPUT_',
                  lengthMenu: '<span>Show:</span> _MENU_',
                  paginate: { 'first': 'First', 'last': 'Last', 'next': ' Next &rarr;', 'previous': '&larr; Preview ' }
                },
                drawCallback: function () {
                  $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                  $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
              });
            /************** Default Settings **************/

            /********** Activated Accounts ************/
              $('#dt-example-responsive').dataTable({
                "order": [[ 0, "desc" ]],
                ajax: {
                  type : 'GET',
                  url : '<?= base_url()?>dashboard/allrequests_json',
                  dataSrc: '',
                  error: function() {
                    alert("error retrieving list");
                  }
                },
                columns: [
                  {data: "id"},
                  {data: "complain"},
                  {data: "subject"},
                  {data: "email"},
                  {data: "date_created"},
                  {data: "department"},
                  {data: "priority"},
                  {data: "assigned_to"},
                  {data: "status", render: function(data,type,row,meta) { 
                    let status = row.status;
                    if(status == 'pending')
                      pending = "selected";
                    else
                    pending = "";

                    if(status == 'Processing')
                      processing = "selected";
                    else
                    processing = "";

                    if(status == 'Resolved')
                      processed = "selected";
                    else
                    processed = "";

                  if(status == 'Escalated (APEX)')
                      escalated = "selected";
                    else
                    escalated = "";

                    return '<div class="form-control-element"><select class="select2 form-control custom-select changestatus" data-id='+window.btoa(row.id)+'><option '+pending+'>Pending</option><option '+processing+'>Processing</option><option '+processed+'>Resolved</option><option '+escalated+'>Escalated (APEX)</option><option>Closed</option></select></div>';
                  }},
                  {render: function(data,type,row,meta) {
                let stripedDescription = escape(row.description);
                    return "<button class='btn btn-primary btn-xs view_req_det' data-t_id='"+row.id+"' data-s_name='"+row.created_by+"' data-s_contact='"+row.sender_contact+"' data-sub='"+row.subject+"' data-desc='"+stripedDescription+"' data-priority='"+row.priority+"' data-d_date='"+row.due_date +"' data-dept='"+row.complain+"' data-assigned_to='"+row.assigned_to+"' data-img='"+row.filepath+"'>Details</button>" +
                    " <button class='btn btn-warning btn-xs forward_to' data-t_id='"+row.id+"'>Forward To</button> "
                  }}
                ],
              });
            /********** Activated Accounts ************/
          });
          //$("#dt-example-responsive").DataTable();
        </script>
 -->
        <!-- Updating Status of Ticket -->
        <script type="text/javascript">
          $(document).on("change",".changestatus",function(){
            let formData = { 
              'ticketid': $(this).data('id'),
              'status': $(this).children('option:selected').text()
            };
            
            $.ajax({
              type : 'POST',
              url : '<?= base_url()?>dashboard/updaterequest',
              data : formData,
              success: function(response) {
                let responseData = JSON.parse(response);
                if(responseData['status'] == 203) {
                  alert(responseData['error']);
                }
                else if(responseData['status'] == 200) {
                  alert(responseData['message'])
                }
                //console.log(responseData);
              },
              error: function() {
                alert("Error Transmitting Data")
              }
            });
          });
        </script>
        <!-- Updating Status of Ticket -->

        <!-- Assignment of Ticket -->
        <script type="text/javascript">
          $(document).on("change",".assigntouser",function(){
            let formData = { 
              'ticketid': $(this).data('id'),
              'assignedto': $(this).children('option:selected').val()
            };
            //console.log(formData);
            $.ajax({
              type : 'POST',
              url : '<?= base_url()?>dashboard/assignedto',
              data : formData,
              success: function(response) {
                let responseData = JSON.parse(response);
                if(responseData['status'] == 203) {
                  alert(responseData['error']);
                }
                else if(responseData['status'] == 200) {
                  alert(responseData['message'])
                }
                //console.log(responseData);
              },
              error: function() {
                alert("Error Transmitting Data")
              }
            });
          });
        </script>
        <!-- Assignment of Ticket -->
    </div>
</div>


<!--Modal Edit-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Info</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="row">
          <div class="col-1">
            <img src="<?=base_url()?>resources/img/logo.png" style="width:140px;">
          </div>
          <div class="col-11">
            <center>
              <h2>OKOMFO ANOKYE RURAL BANK LTD.</h2>
              <h3>E-ZWICH CARD APPLICATION FORM</h3>
            </center>
          </div>
        </div>        

       <legend><h5>PERSONAL DETAILS</h5></legend>
         <form action="<?=base_url()?>access/save_request" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-6">
              
              <div class="form-control-element">

                <input type="text" class="form-control margin-bottom-5" placeholder="Surname (Mr/Mrs/Miss/Other)" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Initials" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="First Name" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="date" class="form-control margin-bottom-5" placeholder="Date of Birth" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>

              <legend><h5>MARITAL STATUS</h5></legend>
              <div class="row">
                <div class="col-4">
                 <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember">
                    <label class="custom-control-label" for="idremember">Single</label>
                </div>
              
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember0">
                    <label class="custom-control-label" for="idremember0">Widow</label>
                </div>
              </div>
                </div>
                 <div class="col-4">
                    <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember1">
                    <label class="custom-control-label" for="idremember1">Married</label>
                </div>
             
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember2">
                    <label class="custom-control-label" for="idremember2">Widower</label>
                </div>
              </div>
                 </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember3">
                    <label class="custom-control-label" for="idremember3">Divorced</label>
                </div>
              </div> 
                  </div>
              </div>
              <div class="row">
                <div class="col-4">
                  <div class="form-group">
                    <h5>GENDER</h5>
                 </div>
                  </div>
                <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember4">
                    <label class="custom-control-label" for="idremember4">Male</label>
                </div>
              </div> 
                  </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-50">
                <input type="checkbox" class="custom-control-input"  id="idremember5">
                    <label class="custom-control-label" for="idremember5">Female</label>
                </div>
              </div> 
                  </div>
              </div>
              <legend><h5>RESIDENTIAL DETAILS</h5></legend>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Street & House No." name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="row">
                <div class="col-6">
                <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Address: P O BOX" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>  
                </div>
                <div class="col-6">
                  <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="City" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
                </div>
              </div>
              <div class="form-control-element margin-bottom-50">
               <select class="select2 form-control custom-select" name="complain" required>
              <option value="" disabled selected>Select Region</option>
              <?php if(!empty($allcomplains)) : foreach($allcomplains as $complain) : ?>
                <option value="<?=$complain->id?>"> <?=$complain->name?> </option>
              <?php endforeach; endif; ?>
              </select>
               </div>
               <div class="row">
                 <div class="col-6">
                  <div class="form-control-element">
                <input type="phone" class="form-control margin-bottom-5" placeholder="Mobile N0." name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div> 
                 </div>
                 <div class="col-6">
                   <div class="form-control-element">
                <input type="Phone" class="form-control margin-bottom-5" placeholder="Home Tel No." name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
                 </div>
               </div>
                <div class="form-control-element">
                <input type="email" class="form-control margin-bottom-5" placeholder="Email" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <legend><h5>PERSONAL IDENTIFICATION</h5></legend>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Drivers License" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Voters ID" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="Date" class="form-control margin-bottom-5" placeholder="Birth Certificate" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="National ID" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Passprot N0." name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="row">
                <div class="col-6">
                   <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Issue Date" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
                </div>
                <div class="col-6">
                   <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Expiry Date" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
                </div>
              </div>
              <legend><h5>CONTACT PERSON</h5></legend>

              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Name" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Address" name="creator_contact" required>
                <div class="form-control-element__box"><span class="fa fa-phone"></span></div>
              </div>
               <div class="form-control-element">
                <input type="phone" class="form-control margin-bottom-5" placeholder="Telephone Number" name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="E ZWICH N0." name="subject" maxlength="30" required>
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
            </div> 


        
        
            <div class="col-6">
              <div class="row">
                <legend><h5>EMPLOYMET DETAILS</h5></legend>
                <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input" id="idremember6">
                    <label class="custom-control-label" for="idremember6">Full Time</label>
                </div>
              </div> 
                  </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input" id="idremember7">
                    <label class="custom-control-label" for="idremember7">Part Time</label>
                </div>
              </div> 
                  </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox ">
                <input type="checkbox" class="custom-control-input"  id="idremember8">
                    <label class="custom-control-label" for="idremember8">Unemployed</label>
                </div>
              </div> 
                  </div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Occupation (current)" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Employer's Name" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Employer's Address" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element margin-bottom-5">
               <select class="select2 form-control custom-select" name="complain" >
              <option value="" disabled selected> Region</option>
              <?php if(!empty($allcomplains)) : foreach($allcomplains as $complain) : ?>
                <option value="<?=$complain->id?>"> <?=$complain->name?> </option>
              <?php endforeach; endif; ?>
              </select>
               </div>
               <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Staff ID No." name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="Date" class="form-control margin-bottom-5" placeholder="Start Date" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <legend><h5>CARD INFORMATION</h5></legend>
              <div class="row">
                <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember9">
                    <label class="custom-control-label" for="idremember9">Current Account</label>
                </div>
              </div> 
                  </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember10">
                    <label class="custom-control-label" for="idremember10">Savings Account</label>
                </div>
              </div> 
                  </div>
              </div>
              <div class="form-control-element">
                <input type="Number" class="form-control margin-bottom-5" placeholder="Account No. 1" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="Number" class="form-control margin-bottom-5" placeholder="Account No. 2" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="Number" class="form-control margin-bottom-5" placeholder="Daily Withdrawl Limit" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="row">
                <h5>Average Income:</h5>
                <div class="col-4">
                 <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember11">
                    <label class="custom-control-label" for="idremember11">0-2000</label>
                </div>
              </div> 
               <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember12">
                    <label class="custom-control-label" for="idremember12">5001-10000</label>
                </div>
              </div>
                </div>
                 <div class="col-4">
                    <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember13">
                    <label class="custom-control-label" for="idremember13">2001-5000</label>
                </div>
              </div> 
               <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember14">
                    <label class="custom-control-label" for="idremember14">10001 and Above</label>
                </div>
              </div>
                 </div>
              </div>
              <legend><h5>OTHER BANKS ACCOUNTS TO BE LINKED TO CARD</h5></legend>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="1 Bank Name" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Name Of Account Holder" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="row">
                <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember15">
                    <label class="custom-control-label" for="idremember15">Current Account</label>
                </div>
              </div> 
                  </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember16">
                    <label class="custom-control-label" for="idremember16">Savings Account</label>
                </div>
              </div> 
                  </div>
              </div>
              <div class="form-control-element">
                <input type="Number" class="form-control margin-bottom-50" placeholder="Account No. 1" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="2 Bank Name" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="form-control-element">
                <input type="text" class="form-control margin-bottom-5" placeholder="Name Of Account Holder" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <div class="row">
                <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember17">
                    <label class="custom-control-label" for="idremember17">Current Account</label>
                </div>
              </div> 
                  </div>
                  <div class="col-4">
                     <div class="form-group">
                <div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
                <input type="checkbox" class="custom-control-input"  id="idremember18">
                    <label class="custom-control-label" for="idremember18">Savings Account</label>
                </div>
              </div> 
                  </div>
              </div>
              <div class="form-control-element">
                <input type="Number" class="form-control margin-bottom-5" placeholder="Account No. 1" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>
              <hr>
              <div class="form-control-element">
                <input type="date" class="form-control margin-bottom-5" placeholder="" name="subject" maxlength="30" >
                <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>