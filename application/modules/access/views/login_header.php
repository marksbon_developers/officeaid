<head>
   <title><?=$title?></title>
   <!-- META SECTION -->
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width,initial-scale=1">
   <link rel="shortcut icon" href="<?=base_url()?>resources/favicon.ico" type="image/x-icon">
   <link rel="icon" href="<?=base_url()?>resources/img/favicon.ico" type="image/x-icon">
   <link rel="stylesheet" href="<?=base_url()?>resources/css/styles2c70.css?v=1.0.3">
   <link href="<?=base_url()?>resources/node_modules/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
   <link href="<?=base_url()?>resources/extentions/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
   <script src="<?=base_url()?>resources/js/vendors/jquery/jquery.min.js"></script>
   <style type="text/css">
      .loader {
         border: 16px solid #f3f3f3; /* Light grey */
         border-top: 16px solid blue;
         border-right: 16px solid green;
         border-bottom: 16px solid red;
         border-left: 16px solid pink;
         border-radius: 50%;
         width: 120px;
         height: 120px;
         animation: spin 2s linear infinite;
      }
      
      @keyframes spin {
         0% { transform: rotate(0deg); }
         100% { transform: rotate(360deg); }
      }
      .transfer-icon {
         width: 200px;         
         height: 120px;
      }
            
      .transfer-icon:hover {
         transform: scale(1.1);
      }

      .data-preview {
         border:0px;
         border-bottom: 1px dashed black; 
         text-align: center;
         font-size: 15px;
         font-weight: bold;
      }
   </style>
</head>
<body>