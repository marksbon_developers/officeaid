<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Remittance extends MX_Controller
{
	/**
	 * Default Page Load
	 * @return view
	 */
	public function index()
	{
		if(/*in_array('Remittance',$_SESSION['user']['roles'])*/ true) :
			# Loading Models
			$this->load->model("globals/model_retrieval");
			$dbres = self::$_Default_DB;

			# Retrieving Company Details
			$tablename = "company_info";
			$where_condition = ['where_condition' => array('id' => 1)];
			$title['companyinfo'] = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$where_condition);

			$title['title'] = "Transfer Forms | OfficeAid";
			$title['pageName'] = "Transfer";

			# loading views
			$this->load->view('access/login_header',$title);
			$this->load->view('nav',$title);
			$this->load->view('transfer', $title);
			$this->load->view('access/login_footer');
		else:
			redirect('access');
		endif;
	}

	public function ezwich()
	{
		# session check
		if(empty($_SESSION['user']))
			redirect('access');

		if(in_array('Remittance',$_SESSION['user']['roles'])) :
			# Loading Models
			$this->load->model("globals/model_retrieval");
			$dbres = self::$_Default_DB;

			# Retrieving Company Details
			$tablename = "company_info";
			$where_condition = ['where_condition' => array('id' => 1)];
			$title['companyinfo'] = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$where_condition);

			$title['title'] = "Remittance Forms | OfficeAid";
			$title['pageName'] = "Remittance";

			# loading views
			$this->load->view('access/login_header',$title);
			$this->load->view('nav',$title);
			$this->load->view('remit', $title);
			$this->load->view('modals');
			$this->load->view('access/login_footer');
		else:
			redirect('access');
		endif;
	}

	public function ezwich_all()
	{
		# session check
		if(empty($_SESSION['user']))
			redirect('access');

		if(in_array('Remittance',@$_SESSION['user']['roles'])) :
			# Loading Models
			$this->load->model("globals/model_retrieval");
			$dbres = self::$_Default_DB;

			# Retrieving Company Details
			$tablename = "company_info";
			$where_condition = ['where_condition' => array('id' => 1)];
			$title['companyinfo'] = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$where_condition);

			$title['title'] = "Transfer Forms | OfficeAid";
			$title['pageName'] = "Transfer";

			# loading views
			$this->load->view('access/login_header',$title);
			$this->load->view('nav',$title);
			$this->load->view('ezwich', $title);
			$this->load->view('access/login_footer');
		else:
			redirect('access');
		endif;
	}

	public function ezwich_json()
	{
	    if(!isset($_SESSION['user']['username']) && !isset($_SESSION['user']['roles']))
      		print "";
    	else
	    {
	      # Loading Models
	      $this->load->model('globals/model_retrieval');

	      $dbres = self::$_Default_DB;
	      $tablename = "remittance";
	      $condition = array(
	        'orderby'=> ['created_at' => "Desc"]
	      );

	      $query_result = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$condition);
	     
	      if($query_result) {
	        foreach ($query_result as $key => $value) {
	          # code...
	          $query_result[$key]->id = str_pad($value->id, 7, 0,STR_PAD_LEFT);
	          
	        $query_result[$key]->created_at = date('Y-m-d',strtotime($value->created_at));
	        }
	      }
	      print_r(json_encode($query_result)); 
	    }
	}

	
	public function ezwich_store()
	{ 
		# session check
		if(empty($_SESSION['user']))
			redirect('access');

	   if(in_array('Remittance',$_SESSION['user']['roles'])) :
			# Processing Required Paramemters
	   	if(!empty($_POST['required'])) {
	   		foreach ($_POST['required'] as $key => $value) {
	   			# Fixing display name
	   			if(strpos($key, '_')) 
	   				$error_display_name = ucwords(str_replace('_', ' ', $key));
	   			else
	   				$error_display_name = ucwords($key);

	   			@$rules[] = array(
	   				'field' => $key,
                	'label' => $error_display_name,
                	'rules' => 'trim|required'
	   			);
	   			# Setting DataSet 
	   			$request_data[$key] = ucwords($value);
	   		}
	   	}

	   	# Processing Option Params
			if(!empty($_POST['optional'])) {
				foreach ($_POST['optional'] as $key => $value) {
					# Fixing display name
					if(strpos($key, '_'))
						$error_display_name = ucwords(str_replace('_', ' ', $key));
					else
						$error_display_name = ucwords($key);

					@$rules[] = array(
						'field' => $key,
						'label' => $error_display_name,
						'rules' => 'trim'
					);
					# Setting DataSet
					$request_data[$key] = ucwords($value);
				}
			}

	   	$this->form_validation->set_data($request_data);
	   	$this->form_validation->set_rules($rules);

	      if($this->form_validation->run() === FALSE) {
	        $errors = str_replace(array("\r","\n","<p>","</p>"),array("<br/>","<br/>","",""),$this->form_validation->error_array());
	        $this->session->set_flashdata('error', implode('<br>', $errors));
	        redirect('remittance/ezwich/new');
	        //print_r($errors); exit;
	      }
	      else {
	      	# Loading Models 
	        	$this->load->model('globals/model_insertion');

	        	/***** Data Definition *****/
	        	$dbres = self::$_Default_DB;
	        	$tablename = "remittance";
	        	$request_data['uuid'] = Remittance::UUID_GEN(25);
	        	$request_data['created_by'] = $_SESSION['user']['id'];
	        /***** Data Definition *****/

	        /******** Insertion Of New Data ***********/
	        $save_data = $this->model_insertion->datainsert($dbres,$tablename,$request_data);
	        if($save_data)
	          $this->session->set_flashdata('success', 'Form Saved Successful');
	        else 
	          $this->session->set_flashdata('error', 'Saving Form Failed');
	        /******** Insertion Of New Data ***********/
				if(isset($_POST['save_print']))
					redirect('remittance/ezwich/preview/'.$save_data);
				else
					redirect('remittance/ezwich/new');
	      }
      else :
        $this->session->set_flashdata('error', 'Saving User Failed');
        redirect('remittance');

      endif;
	}

	public function ezwich_update()
	{ 
		# session check
		if(empty($_SESSION['user']))
			redirect('access');

	   if(in_array('Remittance',$_SESSION['user']['roles'])) :
			# Processing Required Paramemters
	   	if(!empty($_POST['required'])) {
	   		foreach ($_POST['required'] as $key => $value) {
	   			# Fixing display name
	   			if(strpos($key, '_')) 
	   				$error_display_name = ucwords(str_replace('_', ' ', $key));
	   			else
	   				$error_display_name = ucwords($key);

	   			@$rules[] = array(
	   				'field' => $key,
                	'label' => $error_display_name,
                	'rules' => 'trim|required'
	   			);
	   			# Setting DataSet 
	   			$request_data[$key] = ucwords($value);
	   		}
	   	}

	   	# Processing Option Params
			if(!empty($_POST['optional'])) {
				foreach ($_POST['optional'] as $key => $value) {
					# Fixing display name
					if(strpos($key, '_'))
						$error_display_name = ucwords(str_replace('_', ' ', $key));
					else
						$error_display_name = ucwords($key);

					@$rules[] = array(
						'field' => $key,
						'label' => $error_display_name,
						'rules' => 'trim'
					);
					# Setting DataSet
					$request_data[$key] = ucwords($value);
				}
			}

	   	$this->form_validation->set_data($request_data);
	   	$this->form_validation->set_rules($rules);

	      if($this->form_validation->run() === FALSE) {
	        $errors = str_replace(array("\r","\n","<p>","</p>"),array("<br/>","<br/>","",""),$this->form_validation->error_array());
	        $this->session->set_flashdata('error', impldeo('<br>',$errors));
	        redirect('remittance/ezwich/new');
	        //print_r($errors); exit;
	      }
	      else {
	      	# Loading Models 
	      	$this->load->model('globals/model_update');
	        $this->load->model('globals/model_retrieval');

        	/***** Data Definition *****/
        	$dbres = self::$_Default_DB;
        	$tablename = "remittance";
        	$request_data['updated_by'] = $_SESSION['user']['id'];
	        /***** Data Definition *****/

	        /******** Insertion Of New Data ***********/
	        # Getting Department Name
			$search = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,array('where_condition' => array('id' => (int)$this->input->post('request_id'))));

			$where_condition = ['id' => (int)$this->input->post('request_id')];
			$save_data = $this->model_update->update_info($dbres,$tablename,$return_dataType="php_object",$request_data,$where_condition);

	        if($save_data)
	          $this->session->set_flashdata('success', 'Form Updated Successful');
	        else 
	          $this->session->set_flashdata('error', 'Updating Form Failed');
	        /******** Insertion Of New Data ***********/
			
			redirect('remittance/ezwich');
	      }
      else :
        $this->session->set_flashdata('error', 'Saving User Failed');
        redirect('remittance');

      endif;
	}

	public function ezwich_preview($request)
	{
		# session check
		if(empty($_SESSION['user']))
			redirect('access');

		if(in_array('Remittance',$_SESSION['user']['roles'])) :
			# Loading Models
			$this->load->model("globals/model_retrieval");
			$dbres = self::$_Default_DB;

			# Retrieving Company Details
			$tablename = "company_info";
			$where_condition = ['where_condition' => array('id' => 1)];
			$title['companyinfo'] = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$where_condition);

			# Retrieving Record
			$tablename = "remittance";
			$where_condition = ['where_condition' => array('id' => $request)];
			$title['record'] = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$where_condition);

			# page settings
			$title['title'] = "Remittance Forms | OfficeAid";
			$title['pageName'] = "Remittance";

			# loading views
			$this->load->view('access/login_header',$title);
			//$this->load->view('nav',$title);
			$this->load->view('ezwich_print_preview', $title);
			$this->load->view('modals');
			$this->load->view('access/login_footer');
		else:
			redirect('access');
		endif;
	}

	public function ezwich_search($uuid)
	{
	    if(!isset($_SESSION['user']['username']) && !isset($_SESSION['user']['roles']))
      		print "";
    	else
	    {
	      # Loading Models
	      $this->load->model('globals/model_retrieval');

	      $dbres = self::$_Default_DB;
	      $tablename = "remittance";
	      $condition = array(
	      	'where_condition' => ['uuid' => $uuid],
	        'orderby'=> ['created_at' => "Desc"]
	      );

	      $query_result = $this->model_retrieval->retrieve_allinfo($dbres,$tablename,$condition);
	     
	      if($query_result) {
	        foreach ($query_result as $key => $value) {
	          # code...
	          $query_result[$key]->id = str_pad($value->id, 7, 0,STR_PAD_LEFT);
	          
	        $query_result[$key]->created_at = date('Y-m-d',strtotime($value->created_at));
	        }
	      }
	      print_r(json_encode($query_result[0])); 
	    }
	}

	/* UUID Gen*/
	public static function UUID_GEN($lenght = 13) {
	    // uniqid gives 13 chars, but you could adjust it to your needs.
	    if (function_exists("random_bytes")) {
	        $bytes = random_bytes(ceil($lenght / 2));
	    } elseif (function_exists("openssl_random_pseudo_bytes")) {
	        $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
	    } else {
	        throw new Exception("no cryptographically secure random function available");
	    }
	    return substr(bin2hex($bytes), 0, $lenght);
	}
}
