<!--Notification Alert-->
<div class="row">
  <div class="col-md-5"></div>
  <div class="col-md-3" style="text-align: center">
    <?php if($this->session->flashdata('success')) : ?>
      <div class="alert alert-success" role="alert"><strong><?=$this->session->flashdata('success')?></strong></div>
    <?php elseif($this->session->flashdata('error')) : ?>
      <div class="alert alert-danger" role="alert"><strong><?=$this->session->flashdata('error')?> !</strong>  </div>
    <?php endif; ?>
  </div>
  <div class="col-md-4"></div>
</div>

<!--Notification Alert-->

<div class="row">
   <div class="col-md-2 offset-10"> 
      <a href="<?=base_url()?>remittance/ezwich/new"><button class="btn btn-primary"><i class="fa fa-plus"></i> &nbsp; Add New</button></a>
   </div>
</div>
<br>
<div class="card">
   <div class="card-body">
      <table id="all_ezwich_tbl" class="table table-bordered" >
         <thead>
            <tr>
               <th>No #</th>
               <th>Full Name</th>
               <th>Gender</th>
               <th>Region</th>
               <th>Tel.</th>
               <th>ID Type</th>
               <th>ID Number</th>
               <th>Email</th>
               <th>E-Zwich</th>
               <th>Action</th>
            </tr>
         </thead>
         <tbody>
         </tbody>
      </table>

<script type="text/javascript">
   $(document).ready(function(){
      /************** Default Settings **************/
        $.extend( $.fn.dataTable.defaults, {
          autoWidth: false,
          responsive: true,
          columnDefs: [{ 
              orderable: false,
              width: '100px',
          }],
          dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
          language: {
            search: '<span>Filter:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': ' Next &rarr;', 'previous': '&larr; Preview ' }
          },
          drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
          },
          preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
          }
        });
      /************** Default Settings **************/
      $('#all_ezwich_tbl').dataTable({
         "order": [[ 0, "desc" ]],
         ajax: {
            type : 'GET',
            url : '<?= base_url()?>remittance/ezwich_json',
            dataSrc: '',
            error: function() {
               alert("error retrieving list");
            }
         },
         columns: [
            {data: "id"},
            {render:function(data,type,row,meta){
               return row.title + ' ' + row.lastname + ' ' + row.firstname;
            }},
            {data: "gender"},
            {data: "region"},
            {data: "primary_tel"},
            {data: "id_type"},
            {data: "id_number"},
            {data: "email"},
            {data: "contact_enumber"},
            {render: function(data,type,row,meta){
               let action_btn = '<button data-id="'+row.uuid+'" class="btn btn-xs btn-warning getdata"><i class="fa fa-pencil"></i></button> <a href="<?=base_url()?>remittance/ezwich/preview/'+row.id+'" class="btn btn-xs btn-primary "><i class="fa fa-print"></i></a>';

               return action_btn;
            }}
         ],
      });

      /**
       * [class description]
       * @type {String}
       */
      $(document).on('click', '.getdata', function(){
         /* running ajax */
         $.ajax({
            type : 'GET',
            url : '<?=base_url()?>remittance/ezwich_search/'+ $(this).data('id'),
            success: function(response) { 
               response = JSON.parse(response);
             
               // Setting Values 
               $('#request_id').val(response.id);
               $('#title').val(response.title).change();
               $('#surname').val(response.lastname);
               $('#initials').val(response.initials);
               $('#firstname').val(response.firstname);
               $('#dob').val(response.dob);
               $("input[name='required[marital_status]'][value='"+response.marital_status+"']").prop('checked', true);
               $("input[name='required[gender]'][value='"+response.gender+"']").prop('checked', true);

               $('#street_address').val(response.street_address);
               $('#postal_address').val(response.postal_address);
               $('#primary_tel').val(response.primary_tel);
               $('#region').val(response.region).change();
               $('#city').val(response.city);
               $('#secondary_tel').val(response.secondary_tel);
               $('#email').val(response.email);

               $('#id_type').val(response.id_type).change();
               $('#date_issued').val(response.date_issued);
               $('#id_number').val(response.id_number);
               $('#date_expiry').val(response.date_expiry);

               $('#contact_name').val(response.contact_name);
               $('#contact_address').val(response.contact_address);
               $('#contact_phone').val(response.contact_phone);
               $('#contact_enumber').val(response.contact_enumber);

               $("input[name='optional[employment_type]'][value='"+response.employment_type+"']").prop('checked', true);
               $('#occupation').val(response.occupation);
               $('#employer_name').val(response.employer_name);
               $('#employer_address').val(response.employer_address);
               $('#employment_region').val(response.employment_region).change();
               $('#staff_id').val(response.staff_id);
               $('#date_of_employement').val(response.date_of_employement);
               
               $("input[name='optional[account_type]'][value='"+response.account_type+"']").prop('checked', true);
               $('#account_num_1').val(response.account_num_1);
               $('#withdrawal_limit').val(response.withdrawal_limit);
               $('#account_num_2').val(response.account_num_2);

               $('#linked_bank_name_1').val(response.linked_bank_name_1);
               $('#linked_account_number_1').val(response.linked_account_number_1);
               $('#linked_account_name_1').val(response.linked_account_name_1);

               $('#linked_bank_name_2').val(response.linked_bank_name_2);
               $('#linked_account_number_2').val(response.linked_account_number_2);
               $('#linked_account_name_2').val(response.linked_account_name_2);

               $("input[name='optional[average_income]'][value='"+response.average_income+"']").prop('checked', true);
               $("input[name='optional[linked_account_type_1]'][value='"+response.linked_account_type_1+"']").prop('checked', true);
               $("input[name='optional[linked_account_type_2]'][value='"+response.linked_account_type_2+"']").prop('checked', true);
                          },
           error: function() {
             $.jGrowl('An Error Occured.<br/>Please Contact Admin', {
               theme: 'alert-styled-left bg-danger'
             });
           }
         });

         $('.datapreview').modal('show');
      })
   });
</script>

<!-- Modals -->
<div class="modal fade datapreview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg">
     <div class="modal-content" style="width: 1050px;">
       <div class="modal-header">
         <h5 class="modal-title" id="exampleModalLabel">Edit Info</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       </div>
       <div class="modal-body">
         <form action="<?=base_url()?>remittance/ezwich/update" method="post" enctype="multipart/form-data" style="margin: 10px">
          <input type="hidden" name="request_id" id="request_id">
            <div class="row">
               <div class="col-6">
                  <div class="row">
                     <legend><h5>PERSONAL DETAILS<i style="color: red">*</i></h5></legend>
                     <div class="col-6">
                       <div class="row">
                         <div class="col-3"  style="padding-right: 1px">
                          <div class="form-control-element margin-bottom-5">
                           <select id="title" class="select2 form-control custom-select" name="required[title]" required>
                              <option value="" disabled selected>Title</option>
                              <option>Mr</option>
                              <option>Mrs</option>
                              <option>Miss</option>
                           </select>
                        </div> 
                         </div>
                         <div class="col-9" style="padding-left: 1px">
                           <div class="form-control-element">
                           <input id="surname" type="text" class="form-control margin-bottom-5" placeholder="Surname" name="required[lastname]" maxlength="30" required>
                        </div>
                         </div>
                       </div>
                       
                       
                        <div class="form-control-element">
                           <input id="initials" type="text" class="form-control margin-bottom-5" placeholder="Initials" name="required[initials]" maxlength="30" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="firstname" type="text" class="form-control margin-bottom-5" placeholder="First Name" name="required[firstname]" maxlength="30" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="dob" type="text" class="form-control margin-bottom-5" placeholder="Date of Birth" name="required[dob]" onfocus="(this.type='date')" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>
                  
                  
                  <div class="row">
                     <div class="col-md-6">
                        <legend style=""><h5>MARITAL STATUS<i style="color: red">*</i></h5></legend>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="required[marital_status]" value="Single"  id="idremember">
                                 <label class="custom-control-label" for="idremember">Single</label>
                              </div>
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="required[marital_status]" value="Widow" id="idremember0">
                                 <label class="custom-control-label" for="idremember0">Widow</label>
                              </div>
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="required[marital_status]" value="Married" id="idremember1">
                                 <label class="custom-control-label" for="idremember1">Married</label>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="required[marital_status]" value="Widower" id="idremember2">
                                 <label class="custom-control-label" for="idremember2">Widower</label>
                              </div>
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="required[marital_status]" value="Divorced"  id="idremember3">
                                 <label class="custom-control-label" for="idremember3">Divorced</label>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="col-md-6">
                        <legend><h5>GENDER <i style="color: red">*</i></h5></legend>
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" name="required[gender]" value="Male"  id="idremember4">
                           <label class="custom-control-label" for="idremember4">Male</label>
                        </div>
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" name="required[gender]" value="Female" id="idremember5">
                           <label class="custom-control-label" for="idremember5">Female</label>
                        </div>
                     </div>
                    
                  </div>
                  
                  <div class="row">
                     <legend><h5>RESIDENTIAL DETAILS<i style="color: red">*</i></h5></legend>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="street_address" type="text" class="form-control margin-bottom-5" placeholder="Street & House No." name="optional[street_address]" maxlength="30" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="postal_address" type="text" class="form-control margin-bottom-5" placeholder="Address: P O BOX" name="optional[postal_address]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="primary_tel" type="phone" class="form-control margin-bottom-5" placeholder="Mobile No." name="required[primary_tel]" maxlength="15" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-control-element margin-bottom-5">
                           <select id="region" class="select2 form-control custom-select" name="required[region]" required>
                              <option value="" disabled selected>Select Region</option>
                              <option>Greater Accra Region</option>
                              <option>Central Region</option>
                              <option>Western Region</option>
                              <option>Ashanti Region</option>
                              <option>Brong Ahafo Region</option>
                              <option>Eastern Region</option>
                              <option>Northern Region</option>
                              <option>Upper East Region</option>
                              <option>Upper West Region</option>
                              <option>Volta Region</option>
                           </select>
                        </div>
                        <div class="form-control-element">
                           <input id="city" type="text" class="form-control margin-bottom-5" placeholder="City" name="required[city]" maxlength="30" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="secondary_tel" type="Phone" class="form-control margin-bottom-5" placeholder="Home Tel No." name="optional[secondary_tel]" maxlength="15">
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-12">
                        <div class="form-control-element">
                           <input id="email" type="email" class="form-control margin-bottom-5" placeholder="Email" name="required[email]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <legend><h5>PERSONAL IDENTIFICATION<i style="color: red">*</i></h5></legend>
                     <div class="col-6">
                       <div class="form-control-element margin-bottom-5">
                           <select id="id_type" class="select2 form-control custom-select" name="required[id_type]" required>
                              <option value="" disabled selected>Select ID Type</option>
                              <option>Drivers License</option>
                              <option>Voters ID</option>
                              <option>National ID</option>
                              <option>Passport</option>
                              <option>Birth Certicate</option>
                           </select>
                        </div>
                        <div class="form-control-element">
                           <input id="date_issued" type="text" class="form-control margin-bottom-5" placeholder="Issue Date" name="required[date_issued]" onfocus="(this.type='date')" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div> 
                     </div>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="id_number" type="text" class="form-control margin-bottom-5" placeholder="ID Number" name="required[id_number]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="date_expiry" type="text" class="form-control margin-bottom-5" placeholder="Expiry Date" name="required[date_expiry]" onfocus="(this.type='date')" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>
                  
                  <div class="row">
                     <div class="col-6">
                        
                     </div>
                     <div class="col-6">
                        
                     </div>
                  </div>
                  <div class="row">
                     <legend><h5>CONTACT PERSON<i style="color: red">*</i></h5></legend>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="contact_name" type="text" class="form-control margin-bottom-5" placeholder="Name" name="required[contact_name]" maxlength="30" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="contact_address" type="text" class="form-control margin-bottom-5" placeholder="Address" name="required[contact_address]" required>
                           <div class="form-control-element__box"><span class="fa fa-phone"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="contact_phone" type="phone" class="form-control margin-bottom-5" placeholder="Telephone Number" name="required[contact_phone]" maxlength="15" required>
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="contact_enumber" type="text" class="form-control margin-bottom-5" placeholder="E ZWICH N0." name="optional[contact_enumber]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>
                  
               </div>

               <div class="col-6" style="border-left: 1px dashed #31afb4; padding-left: 25px;">
                  <div class="row">
                     <legend><h5>EMPLOYMET DETAILS</h5></legend>
                     <div class="col-4">
                        <div class="form-group">
                           <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" name="optional[employment_type]" value="Full Time" id="idremember6">
                              <label class="custom-control-label" for="idremember6">Full Time</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-4">
                        <div class="form-group">
                           <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" name="optional[employment_type]" value="Part Time" id="idremember7">
                              <label class="custom-control-label" for="idremember7">Part Time</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-4">
                        <div class="form-group">
                           <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" name="optional[employment_type]" value="Unemployed" id="idremember8">
                              <label class="custom-control-label" for="idremember8">Unemployed</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="occupation" type="text" class="form-control margin-bottom-5" placeholder="Occupation (current)" name="optional[occupation]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="employer_name" type="text" class="form-control margin-bottom-5" placeholder="Employer's Name" name="optional[employer_name]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="employer_address" type="text" class="form-control margin-bottom-5" placeholder="Employer's Address" name="optional[employer_address]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-control-element margin-bottom-5">
                           <select id="employment_region" class="select2 form-control custom-select" name="optional[employment_region]" >
                              <option value="" disabled selected>Select Region</option>
                              <option>Greater Accra Region</option>
                              <option>Central Region</option>
                              <option>Western Region</option>
                              <option>Ashanti Region</option>
                              <option>Brong Ahafo Region</option>
                              <option>Eastern Region</option>
                              <option>Northern Region</option>
                              <option>Upper East Region</option>
                              <option>Upper West Region</option>
                              <option>Volta Region</option>
                           </select>
                        </div>
                        <div class="form-control-element">
                           <input id="staff_id" type="text" class="form-control margin-bottom-5" placeholder="Staff ID No." name="optional[staff_id]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="date_of_employement" type="text" class="form-control margin-bottom-5" placeholder="Start Date" name="optional[date_of_employement]" maxlength="30" onfocus="(this.type='date')">
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>
                  
                  <div class="row">
                     <legend><h5>CARD INFORMATION</h5></legend>
                     <div class="col-4">
                        <div class="form-group">
                           <div class="custom-control custom-radio">
                              <input  type="radio" class="custom-control-input" name="optional[account_type]" value="Current Account" id="idremember9">
                              <label class="custom-control-label" for="idremember9">Current Account</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-4">
                        <div class="form-group">
                           <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" name="optional[account_type]" value="Savings Account" id="idremember10">
                              <label class="custom-control-label" for="idremember10">Savings Account</label>
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="account_num_1" type="Number" class="form-control margin-bottom-5" placeholder="Account No. 1" name="optional[account_num_1]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="withdrawal_limit" type="Number" class="form-control margin-bottom-5" placeholder="Daily Withdrawl Limit" name="optional[withdrawal_limit]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="account_num_2" type="Number" class="form-control margin-bottom-5" placeholder="Account No. 2" name="optional[account_num_2]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>
                  
                  <div class="row">
                     <legend><h5>AVERAGE INCOME</h5></legend>
                     <div class="col-4">
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" name="optional[average_income]" value="0-2000" id="idremember11">
                           <label class="custom-control-label" for="idremember11">0-2000</label>
                        </div>
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" name="optional[average_income]" value="5001-10000"  id="idremember12">
                           <label class="custom-control-label" for="idremember12">5001-10000</label>
                        </div>
                     </div>
                     <div class="col-4">
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" name="optional[average_income]" value="2001-5000"  id="idremember13">
                           <label class="custom-control-label" for="idremember13">2001-5000</label>
                        </div>
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" name="optional[average_income]" value="10001 and Above"  id="idremember14">
                           <label class="custom-control-label" for="idremember14">10001 and Above</label>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <legend><h5>1. OTHER BANKS ACCOUNTS TO BE LINKED TO CARD</h5></legend>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="linked_bank_name_1" type="text" class="form-control margin-bottom-5" placeholder="Bank Name" name="optional[linked_bank_name_1]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="linked_account_name_1" type="text" class="form-control margin-bottom-5" placeholder="Name Of Account Holder" name="optional[linked_account_name_1]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="row">
                           <div class="col-6">
                              <div class="custom-control custom-radio">
                                 <input id="linked_account_type_1" type="radio" class="custom-control-input" name="optional[linked_account_type_1]" value="Current Account"  id="idremember15">
                                 <label class="custom-control-label" for="idremember15">Current Account</label>
                              </div>
                           </div>
                           <div class="col-6">
                              <div class="custom-control custom-radio">
                                 <input id="" type="radio" class="custom-control-input" name="optional[linked_account_type_1]" value="Savings Account"   id="idremember16">
                                 <label class="custom-control-label" for="idremember16">Savings Account</label>
                              </div>
                           </div>
                        </div>
                        <div class="form-control-element">
                           <input id="linked_account_number_1" type="Number" class="form-control margin-top-15 margin-bottom-5" placeholder="Account No." name="optional[linked_account_number_1]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                  </div>
                  
                  <div class="row">
                     <legend><h5>2. OTHER BANKS ACCOUNTS TO BE LINKED TO CARD</h5></legend>
                     <div class="col-6">
                        <div class="form-control-element">
                           <input id="linked_bank_name_2" type="text" class="form-control margin-bottom-5" placeholder="Bank Name" name="optional[linked_bank_name_2]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                        <div class="form-control-element">
                           <input id="linked_account_name_2" type="text" class="form-control margin-bottom-5" placeholder="Name Of Account Holder" name="optional[linked_account_name_2]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="row">
                           <div class="col-6">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="optional[linked_account_type_2]" value="Current Account"   id="idremember17">
                                 <label class="custom-control-label" for="idremember17">Current Account</label>
                              </div>
                           </div>
                           <div class="col-6">
                              <div class="custom-control custom-radio">
                                 <input type="radio" class="custom-control-input" name="optional[linked_account_type_2]" value="Savings Account"   id="idremember18">
                                 <label class="custom-control-label" for="idremember18">Savings Account</label>
                              </div>
                           </div>
                        </div>
                        <div class="form-control-element">
                           <input id="linked_account_number_2" type="Number" class="form-control margin-top-15 margin-bottom-5" placeholder="Account No." name="optional[linked_account_number_2]" maxlength="30" >
                           <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
                        </div>
                     </div>
                   </div>
                 
               </div>
               <div class="divider"></div>
            </div>
            <div class="col-4 offset-5">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
              <button class="btn btn-primary">Save changes</button>
            </div>
            
         </form>
       </div>
     </div>
   </div>
 </div>