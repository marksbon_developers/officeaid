<style type="text/css">
   .input-group {
      margin-bottom: 5px;
   }
   .card-body{
      background-color: white !important;
   }
</style>
<div class="card-body">
   <div class="row">
      <div class="col-1">
         <img src="<?=base_url()?>resources/img/logo.png" style="width:140px;">
      </div>
      <div class="col-11">
         <center>
         <h2 style="font-size:40px;">OKOMFO ANOKYE RURAL BANK LTD.</h2>
         <h3>E-ZWICH CARD APPLICATION FORM</h3>
         </center>
      </div>
   </div>
   <?php
   //print_r($record[0]); exit;
   ?>
   
   <form action="#" method="post" enctype="multipart/form-data">
      <div class="row">
         <div class="col-6">
            <legend><h5>PERSONAL DETAILS</h5></legend>
            <div class="input-group"><div class="input-group-prepend" id="basic-addon3">
               <span class="input-group-text" style="border:0px;">Surname (Mr/Mrs/Miss/Other):</span>
            </div>
            <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->title." ".@$record[0]->lastname?>">
         </div>
         <div class="input-group"><div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Initials</span></div><input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3"  value="<?=@$record[0]->initials?>">
      </div>
      <div class="input-group"><div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">First Name</span></div><input type="text"class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->firstname?>">
   </div>
   <div class="input-group"><div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Date of Birth</span></div><input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->dob?>">
</div>
<legend><h5>MARITAL STATUS</h5></legend>
<div class="row">
   <div class="col-4">
      <div class="form-group">
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->marital_status == "Single") : $singlecheck="checked"; else : $singlecheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember" <?=$singlecheck?> readonly>
            <span><i class="fa fa-check-circle custom-control-input"></i></span> <label class="custom-control-label" for="idremember">Single</label>
         </div>
         
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->marital_status == "Widow") : $widowcheck="checked"; else : $widowcheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember0" <?=$widowcheck?> disabled>
            <label class="custom-control-label" for="idremember0">Widow</label>
         </div>
      </div>
   </div>
   <div class="col-4">
      <div class="form-group">
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->marital_status == "Married") : $marriedcheck="checked"; else : $marriedcheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember1" <?=$marriedcheck?> disabled>
            <label class="custom-control-label" for="idremember1">Married</label>
         </div>
         
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->marital_status == "Widower") : $widowercheck="checked"; else : $widowercheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember2" <?=$widowercheck?> disabled>
            <label class="custom-control-label" for="idremember2">Widower</label>
         </div>
      </div>
   </div>
   <div class="col-4">
      <div class="form-group">
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->marital_status == "Divorced") : $divorcedcheck="checked"; else : $divorcedcheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember3" <?=$divorcedcheck?> disabled>
            <label class="custom-control-label" for="idremember3">Divorced</label>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-4">
      <div class="form-group">
         <h5>GENDER</h5>
      </div>
   </div>
   <div class="col-4">
      <div class="form-group">
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->gender == "Male") : $gendercheck="checked"; else : $gendercheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember4" <?=$gendercheck?> disabled>
            <label class="custom-control-label" for="idremember4">Male</label>
         </div>
      </div>
   </div>
   <div class="col-4">
      <div class="form-group">
         <div class="custom-control custom-checkbox ">
            <?php if(@$record[0]->gender == "Female") : $gendercheck="checked"; else : $gendercheck=""; endif; ?>
            <input type="checkbox" class="custom-control-input"  id="idremember5" <?=$gendercheck?> disabled>
            <label class="custom-control-label" for="idremember5">Female</label>
         </div>
      </div>
   </div>
</div>
<legend><h5>RESIDENTIAL DETAILS</h5></legend>
<div class="input-group"><div class="input-group-prepend" id="basic-addon3">
   <span class="input-group-text" style="border: 0px">Street & House No</span>
</div>
<input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->street_address?>">
</div>
<div class="row">
<div class="col-6">
   <div class="input-group"><div class="input-group-prepend" id="basic-addon3">
      <span class="input-group-text" style="border: 0px">Address: P O BOX</span>
   </div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->postal_address?>">
</div>
</div>
<div class="col-6">
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3">
      <span class="input-group-text" style="border: 0px">City</span>
   </div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->city?>">
</div>
</div>
</div>
<div class="input-group">
<div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Region</span></div>
<input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->region?>">
</div>
<div class="row">
<div class="col-6">
<div class="input-group"><div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Mobile No.</span></div>
<input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->primary_tel?>">
</div>
</div>
<div class="col-6">
<div class="input-group"><div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Home Tel No.</span></div>
<input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->secondary_tel?>">
</div>
</div>
</div>
<div class="input-group"><div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Email</span></div>
<input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->email?>">
</div>
<legend><h5>PERSONAL IDENTIFICATION</h5></legend>
<?php 
   switch ($record[0]->id_type) {
      case 'Drivers License':
         # code...
         $driverlicense_num = $record[0]->id_number;
         $issue_date = $record[0]->date_issued;
         $expiry_date = $record[0]->date_expiry;
         break;
      
      case 'Voters ID':
         # code...
         $voters_num = $record[0]->id_number;
         $issue_date = $record[0]->date_issued;
         $expiry_date = $record[0]->date_expiry;
         break;

      case 'National ID':
         # code...
         $national_num = $record[0]->id_number;
         $issue_date = $record[0]->date_issued;
         $expiry_date = $record[0]->date_expiry;
         break;

      case 'Passport':
         # code...
         $passport_num = $record[0]->id_number;
         $issue_date = $record[0]->date_issued;
         $expiry_date = $record[0]->date_expiry;
         break;

      case 'Birth Certicate':
         # code...
         $birthcert_num = $record[0]->id_number;
         $issue_date = $record[0]->date_issued;
         $expiry_date = $record[0]->date_expiry;
         break;


      default:
         # code...
         break;
   }
?>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Drivers License</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$driverlicense_num?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Voters ID</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3"  value="<?=@$voters_num?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Birth Certificate</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$birthcert_num?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">National ID</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$national_num?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Passport N0.</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$passport_num?>">
</div>
<div class="row">
   <div class="col-6">
      <div class="input-group">
         <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Issue Date</span></div>
         <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$issue_date?>">
      </div>
   </div>
   <div class="col-6">
      <div class="input-group">
         <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Expiry Date</span></div>
         <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$expiry_date?>">
      </div>
   </div>
</div>
<legend><h5>CONTACT PERSON</h5></legend>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Name</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->contact_name?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Address</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->contact_address?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Telephone Number</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->contact_phone?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">E ZWICH N0.</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->contact_enumber?>">
</div>
</div>
<div class="col-6">
<div class="row">
<legend><h5>EMPLOYMET DETAILS</h5></legend>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox ">
   <?php if(@$record[0]->account_type == "Full Time") : $fulltime="checked"; else : $fulltime=""; endif; ?>
   <input type="checkbox" class="custom-control-input" id="idremember6" <?=$fulltime?> disabled>
   <label class="custom-control-label" for="idremember6">Full Time</label>
</div>
</div>
</div>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox ">
   <?php if(@$record[0]->account_type == "Part Time") : $parttime="checked"; else : $parttime=""; endif; ?>
   <input type="checkbox" class="custom-control-input" id="idremember7" <?=$parttime?> disabled>
   <label class="custom-control-label" for="idremember7">Part Time</label>
</div>
</div>
</div>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox ">
   <?php if(@$record[0]->account_type == "Current Account") : $unemployed="checked"; else : $unemployed=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember8" <?=$unemployed?> disabled>
   <label class="custom-control-label" for="idremember8">Unemployed</label>
</div>
</div>
</div>
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text"style="border: 0px">Occupation (current)</span></div>
   <input type="text"  class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->occupation?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Employer's Name</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->employer_name?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Employer's Address</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->employer_address?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Region</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->employment_region?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Staff ID No</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->date_of_employement?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Start Date</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->date_of_employement?>">
</div>
<legend><h5>CARD INFORMATION</h5></legend>
<div class="row">
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->account_type == "Current Account") : $account_type_current="checked"; else : $account_type_current=""; endif; ?>
   <input type="checkbox" class="custom-control-input" style="border: 0px"  id="idremember9" <?=$account_type_current?> disabled>
   <label class="custom-control-label" for="idremember9">Current Account</label>
</div>
</div>
</div>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->account_type == "Savings Account") : $account_type_savings="checked"; else : $account_type_savings=""; endif; ?>
   <input type="checkbox" class="custom-control-input" style="border: 0px"  id="idremember10" <?=$account_type_savings?> disabled>
   <label class="custom-control-label" for="idremember10">Savings Account</label>
</div>
</div>
</div>
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Account No. 1</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->account_num_1?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text"style="border: 0px">Account No. 2</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->account_num_2?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Daily Withdrawl Limit</span></div>
   <input type="text"class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->withdrawal_limit?>">
</div>
<div class="row">
<h5>Average Income:</h5>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->average_income == "0-2000") : $_2k="checked"; else : $_2k=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember11" <?=$_2k?> disabled>
   <label class="custom-control-label" for="idremember11">0-2000</label>
</div>
</div>
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->average_income == "5001-10000") : $_10k="checked"; else : $_10k=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember12" <?=$_10k?> disabled>
   <label class="custom-control-label" for="idremember12">5001-10000</label>
</div>
</div>
</div>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->average_income == "2001-5000") : $_5k="checked"; else : $_5k=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember13" <?=$_5k?> disabled>
   <label class="custom-control-label" for="idremember13">2001-5000</label>
</div>
</div>
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->average_income == "10001 and Above") : $_10k_plus="checked"; else : $_10k_plus=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember14" <?=$_10k_plus?> disabled>
   <label class="custom-control-label" for="idremember14">10001 and Above</label>
</div>
</div>
</div>
</div>
<legend><h5>OTHER BANKS ACCOUNTS TO BE LINKED TO CARD</h5></legend>
<p><strong>1.</strong></p>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Bank Name</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3"  value="<?=@$record[0]->linked_bank_name_1?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Name Of Account Holder</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->linked_account_name_1?>">
</div>
<div class="row">
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->linked_account_type_1 == "Current Account") : $linked_account_type_1_current="checked"; else : $linked_account_type_1_current=""; endif; ?>
   <input type="checkbox" style="border: 0px" class="custom-control-input"  id="idremember15" <?=$linked_account_type_1_current?> disabled>
   <label class="custom-control-label" for="idremember15">Current Account</label>
</div>
</div>
</div>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->linked_account_type_1 == "Savings Account") : $linked_account_type_1_savings="checked"; else : $linked_account_type_1_savings=""; endif; ?>
   <input type="checkbox" style="border: 0px" class="custom-control-input"  id="idremember16" <?=$linked_account_type_1_savings?> disabled>
   <label class="custom-control-label" for="idremember16">Savings Account</label>
</div>
</div>
</div>
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Account No. 1</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->linked_account_number_1?>">
</div>
<p><strong>2.</strong></p>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Bank Name</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->linked_bank_name_2?>">
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Name Of Account Holder</span></div>
   <input type="text"class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->linked_account_name_2?>">
</div>
<div class="row">
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->linked_account_type_2 == "Current Account") : $linked_account_type_2_current="checked"; else : $linked_account_type_2_current=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember17" <?=$linked_account_type_2_current?> disabled>
   <label class="custom-control-label" for="idremember17">Current Account</label>
</div>
</div>
</div>
<div class="col-4">
<div class="form-group">
<div class="custom-control custom-checkbox margin-top-2 margin-bottom-5">
   <?php if(@$record[0]->linked_account_type_2 == "Savings Account") : $linked_account_type_2_savings="checked"; else : $linked_account_type_2_savings=""; endif; ?>
   <input type="checkbox" class="custom-control-input"  id="idremember18" <?=$linked_account_type_2_savings?> disabled>
   <label class="custom-control-label" for="idremember18">Savings Account</label>
</div>
</div>
</div>
</div>
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Account No. 1</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=@$record[0]->linked_account_number_2?>">
</div>
<br>
<div class="row">
<div class="col-6">
<div class="input-group">
   <div class="input-group-prepend" id="basic-addon3"><span class="input-group-text" style="border: 0px">Date of Registration</span></div>
   <input type="text" class="form-control data-preview" id="basic-url" aria-describedby="basic-addon3" value="<?=date('Y-m-d', strtotime(@$record[0]->created_at))?>">
</div>
</div>
<div class="col-6">
<div class="form-control-element">
<label>Customer's Signature</label>
<input type="text" style="border: 0px; height: 60px; border-bottom: 1px solid black" class="form-control">
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
   window.print();
   var delay = 1000;
   var url = '<?=base_url()?>remittance/ezwich'
   setTimeout(function(){ window.location = url; }, delay);
</script>