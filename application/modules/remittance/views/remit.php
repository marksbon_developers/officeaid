<div class="row">
   <div class="col-1">
      <img src="">
   </div>
   <div class="col-11">
      <center>
      <h2>OKOMFO ANOKYE RURAL BANK LTD.</h2>
      <h5><u>E-ZWICH CARD APPLICATION FORM</u></h5>
      </center>
   </div>
</div>

<!--Notification Alert-->
<div class="row">
	<div class="col-md-5"></div>
	<div class="col-md-3" style="text-align: center">
		<?php if($this->session->flashdata('success')) : ?>
			<div class="alert alert-success" role="alert"><strong><?=$this->session->flashdata('success')?></strong></div>
		<?php elseif($this->session->flashdata('error')) : ?>
			<div class="alert alert-danger" role="alert"><strong><?=$this->session->flashdata('error')?> !</strong>  </div>
		<?php endif; ?>
	</div>
	<div class="col-md-4"></div>
</div>

<!--Notification Alert-->

<form action="<?=base_url()?>remittance/ezwich/store" method="post" enctype="multipart/form-data">
   <div class="row">
      <div class="col-6">
         <div class="row">
            <legend><h5>PERSONAL DETAILS<i style="color: red">*</i></h5></legend>
            <div class="col-6">
              <div class="row">
                <div class="col-3"  style="padding-right: 1px">
                 <div class="form-control-element margin-bottom-5">
                  <select class="select2 form-control custom-select" name="required[title]" required>
                     <option value="" disabled selected>Title</option>
                     <option>Mr</option>
                     <option>Mrs</option>
                     <option>Miss</option>
                  </select>
               </div> 
                </div>
                <div class="col-9" style="padding-left: 1px">
                  <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Surname" name="required[lastname]" maxlength="30" required>
               </div>
                </div>
              </div>
              
              
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Initials" name="required[initials]" maxlength="30" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="First Name" name="required[firstname]" maxlength="30" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Date of Birth" name="required[dob]" onfocus="(this.type='date')" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>
         
         
         <div class="row">
            <div class="col-md-6">
               <legend style=""><h5>MARITAL STATUS<i style="color: red">*</i></h5></legend>
               <div class="row">
                  <div class="col-md-6">
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="required[marital_status]" value="Single"  id="idremember" required>
                        <label class="custom-control-label" for="idremember">Single</label>
                     </div>
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="required[marital_status]" value="Widow" id="idremember0">
                        <label class="custom-control-label" for="idremember0">Widow</label>
                     </div>
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="required[marital_status]" value="Married" id="idremember1">
                        <label class="custom-control-label" for="idremember1">Married</label>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="required[marital_status]" value="Widower" id="idremember2">
                        <label class="custom-control-label" for="idremember2">Widower</label>
                     </div>
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="required[marital_status]" value="Divorced"  id="idremember3">
                        <label class="custom-control-label" for="idremember3">Divorced</label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <legend><h5>GENDER <i style="color: red">*</i></h5></legend>
               <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="required[gender]" value="Male"  id="idremember4" required>
                  <label class="custom-control-label" for="idremember4">Male</label>
               </div>
               <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="required[gender]" value="Female" id="idremember5">
                  <label class="custom-control-label" for="idremember5">Female</label>
               </div>
            </div>
           
         </div>
         
         <div class="row">
            <legend><h5>RESIDENTIAL DETAILS<i style="color: red">*</i></h5></legend>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Street & House No." name="optional[street_address]" maxlength="60">
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Address: P O BOX" name="optional[postal_address]" maxlength="60" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="phone" class="form-control margin-bottom-5" placeholder="Mobile No." name="required[primary_tel]" maxlength="15" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="form-control-element margin-bottom-5">
                  <select class="select2 form-control custom-select" name="required[region]" required>
                     <option value="" disabled selected>Select Region</option>
                     <option>Greater Accra Region</option>
                     <option>Central Region</option>
                     <option>Western Region</option>
                     <option>Ashanti Region</option>
                     <option>Brong Ahafo Region</option>
                     <option>Eastern Region</option>
                     <option>Northern Region</option>
                     <option>Upper East Region</option>
                     <option>Upper West Region</option>
                     <option>Volta Region</option>
                  </select>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="City" name="required[city]" maxlength="60" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="Phone" class="form-control margin-bottom-5" placeholder="Home Tel No." name="optional[secondary_tel]" maxlength="15">
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-12">
               <div class="form-control-element">
                  <input type="email" class="form-control margin-bottom-5" placeholder="Email" name="required[email]" maxlength="60" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>

         <div class="row">
            <legend><h5>PERSONAL IDENTIFICATION<i style="color: red">*</i></h5></legend>
            <div class="col-6">
              <div class="form-control-element margin-bottom-5">
                  <select class="select2 form-control custom-select" name="required[id_type]" required>
                     <option value="" disabled selected>Select ID Type</option>
                     <option>Drivers License</option>
                     <option>Voters ID</option>
                     <option>National ID</option>
                     <option>Passport</option>
                     <option>Birth Certicate</option>
                  </select>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Issue Date" name="required[date_issued]" onfocus="(this.type='date')" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div> 
            </div>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="ID Number" name="required[id_number]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Expiry Date" name="required[date_expiry]" onfocus="(this.type='date')" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-6">
               
            </div>
            <div class="col-6">
               
            </div>
         </div>
         <div class="row">
            <legend><h5>CONTACT PERSON<i style="color: red">*</i></h5></legend>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Name" name="required[contact_name]" maxlength="30" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Address" name="required[contact_address]" required>
                  <div class="form-control-element__box"><span class="fa fa-phone"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="phone" class="form-control margin-bottom-5" placeholder="Telephone Number" name="required[contact_phone]" maxlength="15" required>
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="E ZWICH N0." name="optional[contact_enumber]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>
         
      </div>

      <div class="col-6" style="border-left: 1px dashed #31afb4; padding-left: 25px;">
         <div class="row">
            <legend><h5>EMPLOYMET DETAILS</h5></legend>
            <div class="col-4">
               <div class="form-group">
                  <div class="custom-control custom-radio">
                     <input type="radio" class="custom-control-input" name="optional[employment_type]" value="Full Time" id="idremember6">
                     <label class="custom-control-label" for="idremember6">Full Time</label>
                  </div>
               </div>
            </div>
            <div class="col-4">
               <div class="form-group">
                  <div class="custom-control custom-radio">
                     <input type="radio" class="custom-control-input" name="optional[employment_type]" value="Part Time" id="idremember7">
                     <label class="custom-control-label" for="idremember7">Part Time</label>
                  </div>
               </div>
            </div>
            <div class="col-4">
               <div class="form-group">
                  <div class="custom-control custom-radio">
                     <input type="radio" class="custom-control-input" name="optional[employment_type]" value="Unemployed" id="idremember8">
                     <label class="custom-control-label" for="idremember8">Unemployed</label>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Occupation (current)" name="optional[occupation]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Employer's Name" name="optional[employer_name]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Employer's Address" name="optional[employer_address]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="form-control-element margin-bottom-5">
                  <select class="select2 form-control custom-select" name="optional[employment_region]" >
                     <option value="" disabled selected>Select Region</option>
                     <option>Greater Accra Region</option>
                     <option>Central Region</option>
                     <option>Western Region</option>
                     <option>Ashanti Region</option>
                     <option>Brong Ahafo Region</option>
                     <option>Eastern Region</option>
                     <option>Northern Region</option>
                     <option>Upper East Region</option>
                     <option>Upper West Region</option>
                     <option>Volta Region</option>
                  </select>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Staff ID No." name="optional[staff_id]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Start Date" name="optional[date_of_employement]" maxlength="30" onfocus="(this.type='date')">
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <legend><h5>CARD INFORMATION</h5></legend>
            <div class="col-4">
               <div class="form-group">
                  <div class="custom-control custom-radio">
                     <input type="radio" class="custom-control-input" name="optional[account_type]" value="Current Account" id="idremember9">
                     <label class="custom-control-label" for="idremember9">Current Account</label>
                  </div>
               </div>
            </div>
            <div class="col-4">
               <div class="form-group">
                  <div class="custom-control custom-radio">
                     <input type="radio" class="custom-control-input" name="optional[account_type]" value="Savings Account" id="idremember10">
                     <label class="custom-control-label" for="idremember10">Savings Account</label>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
            <div class="col-6">
               <div class="form-control-element">
                  <input type="Number" class="form-control margin-bottom-5" placeholder="Account No. 1" name="optional[account_num_1]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="Number" class="form-control margin-bottom-5" placeholder="Daily Withdrawl Limit" name="optional[withdrawal_limit]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="Number" class="form-control margin-bottom-5" placeholder="Account No. 2" name="optional[account_num_2]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <legend><h5>AVERAGE INCOME</h5></legend>
            <div class="col-4">
               <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="optional[average_income]" value="0-2000" id="idremember11">
                  <label class="custom-control-label" for="idremember11">0-2000</label>
               </div>
               <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="optional[average_income]" value="5001-10000"  id="idremember12">
                  <label class="custom-control-label" for="idremember12">5001-10000</label>
               </div>
            </div>
            <div class="col-4">
               <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="optional[average_income]" value="2001-5000"  id="idremember13">
                  <label class="custom-control-label" for="idremember13">2001-5000</label>
               </div>
               <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" name="optional[average_income]" value="10001 and Above"  id="idremember14">
                  <label class="custom-control-label" for="idremember14">10001 and Above</label>
               </div>
            </div>
         </div>

         <div class="row">
            <legend><h5>1. OTHER BANKS ACCOUNTS TO BE LINKED TO CARD</h5></legend>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Bank Name" name="optional[linked_bank_name_1]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Name Of Account Holder" name="optional[linked_account_name_1]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="row">
                  <div class="col-6">
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="optional[linked_account_type_1]" value="Current Account"  id="idremember15">
                        <label class="custom-control-label" for="idremember15">Current Account</label>
                     </div>
                  </div>
                  <div class="col-6">
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="optional[linked_account_type_1]" value="Savings Account"   id="idremember16">
                        <label class="custom-control-label" for="idremember16">Savings Account</label>
                     </div>
                  </div>
               </div>
               <div class="form-control-element">
                  <input type="Number" class="form-control margin-top-15 margin-bottom-5" placeholder="Account No." name="optional[linked_account_number_1]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
         </div>
         
         <div class="row">
            <legend><h5>2. OTHER BANKS ACCOUNTS TO BE LINKED TO CARD</h5></legend>
            <div class="col-6">
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Bank Name" name="optional[linked_bank_name_2]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
               <div class="form-control-element">
                  <input type="text" class="form-control margin-bottom-5" placeholder="Name Of Account Holder" name="optional[linked_account_name_2]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="col-6">
               <div class="row">
                  <div class="col-6">
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="optional[linked_account_type_2]" value="Current Account"   id="idremember17">
                        <label class="custom-control-label" for="idremember17">Current Account</label>
                     </div>
                  </div>
                  <div class="col-6">
                     <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="optional[linked_account_type_2]" value="Savings Account"   id="idremember18">
                        <label class="custom-control-label" for="idremember18">Savings Account</label>
                     </div>
                  </div>
               </div>
               <div class="form-control-element">
                  <input type="Number" class="form-control margin-top-15 margin-bottom-5" placeholder="Account No." name="optional[linked_account_number_2]" maxlength="30" >
                  <div class="form-control-element__box"><span class="fa fa-pencil"></span></div>
               </div>
            </div>
            <div class="divider"></div>
          </div>
          <div class="row">
            <div class="col-6">
            <button type="submit" class="btn btn-primary btn-block" name="save" id="new_request_submit">Save</button>
            </div>
            <div class="col-6">
					<button type="submit" class="btn btn-primary btn-block" name="save_print" id="new_request_submit">Save & Print</button>
            </div>
          </div>
          
        </form>
      </div>
      <div class="divider"></div>
   </div>
</form>
</div>
</div><!-- PAGE LOGIN CONTAINER -->
</div><!-- //END PAGE CONTENT -->
</div>
<div class="modal fade loading_screen" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">
   <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Awaiting</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <div class="modal-body">
      <div class="loader" style="margin-left: 25%;"></div>
   </div>
</div>
</div>
</div>
<div class="modal fade ticket_result" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">
   <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
      <button type="button" class="close close_ticket_modal" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <div class="modal-body">
      <Strong class="ticketNo"></Strong>
   </div><div class="modal-footer">
   <button type="button" class="btn btn-primary close_ticket_modal" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div><!-- //END PAGE CONTENT CONTAINER -->
</div>
<!-- //END PAGE CONTENT -->
</div>
