<div class="row">
   <div class="col-1">
      <img src="">
   </div>
   <div class="col-11">
      <center>
      <h2>OKOMFO ANOKYE RURAL BANK LTD.</h2>
      </center>
   </div>
</div>

	<div class="row">
		<div class="col-md-2 offset-2">
			<a href="<?=base_url()?>remittance/ezwich">
				<img class="transfer-icon" src="<?=base_url()?>/resources/img/transfer-forms/e-zwich.png">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?=base_url()?>remittance/ria">
				<img class="transfer-icon" src="<?=base_url()?>/resources/img/transfer-forms/ria-money-transfer-logo.png">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?=base_url()?>remittance/unitylink">
				<img class="transfer-icon" src="<?=base_url()?>/resources/img/transfer-forms/unity-link.jpg">
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2 offset-2">
			<a href="<?=base_url()?>remittance/westernunion">
				<img class="transfer-icon" src="<?=base_url()?>/resources/img/transfer-forms/western-union.jpg">
			</a>
		</div>
		<div class="col-md-2">
			<a href="<?=base_url()?>remittance//apexlink">
				<img class="transfer-icon" src="<?=base_url()?>/resources/img/transfer-forms/apex-link.jpg">
			</a>
		</div>
	</div>
      </div>
      <div class="divider"></div>
   </div>
</form>
</div>
</div><!-- PAGE LOGIN CONTAINER -->
</div><!-- //END PAGE CONTENT -->
</div>
<div class="modal fade loading_screen" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">
   <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Awaiting</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <div class="modal-body">
      <div class="loader" style="margin-left: 25%;"></div>
   </div>
</div>
</div>
</div>
<div class="modal fade ticket_result" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">
   <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
      <button type="button" class="close close_ticket_modal" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>
   <div class="modal-body">
      <Strong class="ticketNo"></Strong>
   </div><div class="modal-footer">
   <button type="button" class="btn btn-primary close_ticket_modal" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
</div><!-- //END PAGE CONTENT CONTAINER -->
</div>
<!-- //END PAGE CONTENT -->
</div>
